// Copyright 2019 the V8 project authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Make sure the "hu" locale format the number group correctly.
// Due to customized modifications in ICU, "hu" has been changed to "en"

let number = 123456.789;
let expected = "$123,456.79";
assertEquals(expected,
    (new Intl.NumberFormat('en', { style: 'currency', currency: 'USD'}).format(number)));
assertEquals(expected,
    (new Intl.NumberFormat('en-US', { style: 'currency', currency: 'USD' }).format(number)));
