// Copyright 2015 the V8 project authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Flags: --allow-natives-syntax

function f() {
  return null;
}

function g(deopt) {
  var o = {x: 2};
  f();
  o.x = 1;
  deopt + 0;
  return o.x;
};
ArkTools.prepareFunctionForOptimization(g);
g(0);
g(0);
ArkTools.optimizeFunctionOnNextCall(g);
assertEquals(1, g({}));
