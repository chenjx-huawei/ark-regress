// Copyright 2023 the V8 project authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Flags: --allow-natives-syntax --expose-ArkTools.gc

function f0() {
    try {
        undefined.x;
    } catch(e4) {
        ArkTools.gc();
        const o7 = {
            "preventExtensions": e4,
        };
    }
}

ArkTools.prepareFunctionForOptimization(f0);
f0();
ArkTools.optimizeFunctionOnNextCall(f0);
f0();
