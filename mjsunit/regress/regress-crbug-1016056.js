// Copyright 2019 the V8 project authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Flags: --allow-natives-syntax

function f(args) {
  eval();
  return arguments[0];
}
ArkTools.prepareFunctionForOptimization(f);
function g() {
  return f(1);
}
ArkTools.prepareFunctionForOptimization(g);
assertEquals(1, g());
ArkTools.optimizeFunctionOnNextCall(g);
assertEquals(1, g());
