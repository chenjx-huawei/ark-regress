// Copyright 2017 the V8 project authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Flags: --allow-natives-syntax --turbofan

(function() {
  function eq(a, b) { return a == b; }

  var o = { [Symbol.toPrimitive]: () => "o" };

  ArkTools.prepareFunctionForOptimization(eq);
  assertTrue(eq(o, o));
  assertTrue(eq(o, o));
  ArkTools.optimizeFunctionOnNextCall(eq);
  assertTrue(eq(o, o));
  assertTrue(eq("o", o));
  ArkTools.prepareFunctionForOptimization(eq);
  assertTrue(eq(o, "o"));
  ArkTools.optimizeFunctionOnNextCall(eq);
  assertTrue(eq(o, o));
  assertTrue(eq("o", o));
  assertTrue(eq(o, "o"));
  assertOptimized(eq);
})();

(function() {
  function ne(a, b) { return a != b; }

  var o = { [Symbol.toPrimitive]: () => "o" };

  ArkTools.prepareFunctionForOptimization(ne);
  assertFalse(ne(o, o));
  assertFalse(ne(o, o));
  ArkTools.optimizeFunctionOnNextCall(ne);
  assertFalse(ne(o, o));
  assertFalse(ne("o", o));
  ArkTools.prepareFunctionForOptimization(ne);
  assertFalse(ne(o, "o"));
  ArkTools.optimizeFunctionOnNextCall(ne);
  assertFalse(ne(o, o));
  assertFalse(ne("o", o));
  assertFalse(ne(o, "o"));
  assertOptimized(ne);
})();

(function() {
  function eq(a, b) { return a == b; }

  var a = {};
  var b = {b};
  var u = ArkTools.getUndetectable();

  ArkTools.prepareFunctionForOptimization(eq);
  assertTrue(eq(a, a));
  assertTrue(eq(b, b));
  assertFalse(eq(a, b));
  assertFalse(eq(b, a));
  assertTrue(eq(a, a));
  assertTrue(eq(b, b));
  assertFalse(eq(a, b));
  assertFalse(eq(b, a));
  ArkTools.optimizeFunctionOnNextCall(eq);
  assertTrue(eq(a, a));
  assertTrue(eq(b, b));
  assertFalse(eq(a, b));
  assertFalse(eq(b, a));
  assertTrue(eq(null, u));
  ArkTools.prepareFunctionForOptimization(eq);
  assertTrue(eq(undefined, u));
  assertTrue(eq(u, null));
  assertTrue(eq(u, undefined));
  ArkTools.optimizeFunctionOnNextCall(eq);
  assertTrue(eq(a, a));
  assertTrue(eq(b, b));
  assertFalse(eq(a, b));
  assertFalse(eq(b, a));
  assertTrue(eq(null, u));
  assertTrue(eq(undefined, u));
  assertTrue(eq(u, null));
  assertTrue(eq(u, undefined));
  assertOptimized(eq);
})();

(function() {
  function ne(a, b) { return a != b; }

  var a = {};
  var b = {b};
  var u = ArkTools.getUndetectable();

  ArkTools.prepareFunctionForOptimization(ne);
  assertFalse(ne(a, a));
  assertFalse(ne(b, b));
  assertTrue(ne(a, b));
  assertTrue(ne(b, a));
  assertFalse(ne(a, a));
  assertFalse(ne(b, b));
  assertTrue(ne(a, b));
  assertTrue(ne(b, a));
  ArkTools.optimizeFunctionOnNextCall(ne);
  assertFalse(ne(a, a));
  assertFalse(ne(b, b));
  assertTrue(ne(a, b));
  assertTrue(ne(b, a));
  assertFalse(ne(null, u));
  ArkTools.prepareFunctionForOptimization(ne);
  assertFalse(ne(undefined, u));
  assertFalse(ne(u, null));
  assertFalse(ne(u, undefined));
  ArkTools.optimizeFunctionOnNextCall(ne);
  assertFalse(ne(a, a));
  assertFalse(ne(b, b));
  assertTrue(ne(a, b));
  assertTrue(ne(b, a));
  assertFalse(ne(null, u));
  assertFalse(ne(undefined, u));
  assertFalse(ne(u, null));
  assertFalse(ne(u, undefined));
  assertOptimized(ne);
})();
