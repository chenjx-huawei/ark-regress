// Copyright 2023 the V8 project authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Flags: --allow-natives-syntax

function foo(b) {
    let x = 3.1;
    if(b) x = 1;
    return x ** Infinity;
}

ArkTools.prepareFunctionForOptimization(foo);
assertEquals(NaN, foo(true));
assertEquals(Infinity, foo(false));
ArkTools.optimizeFunctionOnNextCall(foo);
assertEquals(NaN, foo(true));
assertEquals(Infinity, foo(false));
