// Copyright 2015 the V8 project authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Flags: --allow-natives-syntax

var a = {
  y: 1.5
};
a.y = 1093445778;
var b = a.y;
var c = {
  y: {}
};

function f() {
  return {y: b};
};
ArkTools.prepareFunctionForOptimization(f);
f();
f();
ArkTools.optimizeFunctionOnNextCall(f);
assertEquals(f().y, 1093445778);
