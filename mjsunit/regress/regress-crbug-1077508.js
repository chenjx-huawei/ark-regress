// Copyright 2020 the V8 project authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

const array = [, , , 0, 1, 2];
var canDefineProperty = true;
const comparefn = () => {
  // Array.prototype.__defineSetter__("0", function () {});
  // Array.prototype.__defineSetter__("1", function () {});
  // Array.prototype.__defineSetter__("2", function () {});
  // Object.defineProperty redefine protect
  if (canDefineProperty) {
    Object.defineProperty(Array.prototype, '0', { set() {} });
    Object.defineProperty(Array.prototype, '1', { set() {} });
    Object.defineProperty(Array.prototype, '2', { set() {} });
    canDefineProperty = false;
  }
};

array.sort(comparefn);

assertArrayEquals([, , , , , ,], array);
