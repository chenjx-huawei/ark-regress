// Copyright 2014 the V8 project authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Flags: --allow-natives-syntax

// Test push double as tagged.
var a = [{}];
function f(a) {
  a.push(Infinity);
};
ArkTools.prepareFunctionForOptimization(f);
f(a);
f(a);
f(a);
ArkTools.optimizeFunctionOnNextCall(f);
f(a);
assertEquals([{}, Infinity, Infinity, Infinity, Infinity], a);
