// Copyright 2016 the V8 project authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Flags: --allow-natives-syntax

var f = (function() {
  "use asm";
  function foo(x) {
    return x < x;
  }
  return foo;
})();

ArkTools.prepareFunctionForOptimization(f);

function deopt(f) {
  return {
    toString: function() {
      ArkTools.deoptimizeFunction(f);
      return "2";
    }
  };
}

ArkTools.optimizeFunctionOnNextCall(f);
assertFalse(f(deopt(f)));
