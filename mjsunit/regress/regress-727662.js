// Copyright 2017 the V8 project authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Flags: --allow-natives-syntax --expose-ArkTools.gc

(function() {
function thingo(i, b) {
  var s = b ? "ac" : "abcd";
  i = i >>> 0;
  if (i < s.length) {
    var c = s.charCodeAt(i);
    ArkTools.gc();
    return c;
  }
};
ArkTools.prepareFunctionForOptimization(thingo);
thingo(0, true);
thingo(0, true);
ArkTools.optimizeFunctionOnNextCall(thingo);
thingo(0, true);
})();
