// Copyright 2023 the V8 project authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Flags: --allow-natives-syntax

(function TestRegularFunction() {
  function f(a) {
    a.stack = "boom";
  }
  ArkTools.prepareFunctionForOptimization(f);
  f(f);
})();

(function TestFunctionConstructor() {
  function f(a) {
    a.stack = "boom";
  }
  ArkTools.prepareFunctionForOptimization(f);
  f(Function);
})();

(function TestArrowFunction() {
  function f(a) {
    a.stack = "boom";
  }
  ArkTools.prepareFunctionForOptimization(f);

  const arrow = a => {};

  f(arrow);
})();

(function TestClass() {
  function f(a) {
    a.stack = "boom";
  }
  ArkTools.prepareFunctionForOptimization(f);

  class A {};

  f(A);
})();

(function TestStaticMethod() {
  function f(a) {
    a.stack = "boom";
  }
  ArkTools.prepareFunctionForOptimization(f);

  class A {
    static m() {}
  };

  f(A.m);
})();
