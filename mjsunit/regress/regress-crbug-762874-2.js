// Copyright 2017 the V8 project authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Flags: --allow-natives-syntax

const maxLength = ArkTools.stringMaxLength();
const s = 'A'.repeat(maxLength);

function foo(s) {
  let x = s.lastIndexOf("", maxLength);
  return x === maxLength;
};
ArkTools.prepareFunctionForOptimization(foo);
assertTrue(foo(s));
assertTrue(foo(s));
ArkTools.optimizeFunctionOnNextCall(foo);
assertTrue(foo(s));
