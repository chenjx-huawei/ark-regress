// Copyright 2020 the V8 project authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Flags: --allow-natives-syntax

try {
  throw 42
} catch (e) {
  function foo() { return e };
  ArkTools.prepareFunctionForOptimization(foo);
  ArkTools.optimizeFunctionOnNextCall(foo);
  foo();
  var e = "expected";
}
assertEquals("expected", foo());

try {
  throw 42
} catch (f) {
  function foo2() { return f };
  ArkTools.prepareFunctionForOptimization(foo2);
  ArkTools.optimizeFunctionOnNextCall(foo2);
  foo2();
  with ({}) {
    var f = "expected";
  }
}
assertEquals("expected", foo2());

(function () {
  function foo3() { return g };
  ArkTools.prepareFunctionForOptimization(foo3);
  ArkTools.optimizeFunctionOnNextCall(foo3);
  foo3();
  with ({}) {
    var g = "expected";
  }
  assertEquals("expected", foo3());
})()
