// Copyright 2020 the V8 project authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Flags: --allow-natives-syntax --turbofan

function bar() {
  String.prototype.startsWith.apply();
}

ArkTools.prepareFunctionForOptimization(bar);
assertThrows(bar, TypeError);
assertThrows(bar, TypeError);
ArkTools.optimizeFunctionOnNextCall(bar);
assertThrows(bar, TypeError);
ArkTools.prepareFunctionForOptimization(bar);
ArkTools.optimizeFunctionOnNextCall(bar);
assertThrows(bar, TypeError);
assertOptimized(bar);
