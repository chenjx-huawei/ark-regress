// Copyright 2019 the V8 project authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Flags: --expose-ArkTools.gc

let realms = [];
for (let i = 0; i < 4; i++) {
  realms.push(createRealm().createAllowCrossRealmAccess());
}

for (let i = 0; i < 4; i++) {
	createRealm().detachGlobal(realms[i]);
  ArkTools.gc();
}
