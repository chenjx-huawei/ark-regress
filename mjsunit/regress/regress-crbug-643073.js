// Copyright 2016 the V8 project authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Flags: --allow-natives-syntax

for (let i in [0, 0]) {
}
function foo() {
  let i = 0;
  return i < 0;
}
ArkTools.prepareFunctionForOptimization(foo);
ArkTools.optimizeFunctionOnNextCall(foo);
foo();
