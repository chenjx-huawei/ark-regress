// Copyright 2017 the V8 project authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Flags: --allow-natives-syntax

(function TestNonCallableForEach() {
  function foo() {
    [].forEach(undefined);
  };
  ArkTools.prepareFunctionForOptimization(foo);
  assertThrows(foo, TypeError);
  assertThrows(foo, TypeError);
  ArkTools.optimizeFunctionOnNextCall(foo);
  assertThrows(foo, TypeError);
})();

(function TestNonCallableForEachCaught() {
  function foo() {
    try {
      [].forEach(undefined);
    } catch (e) {
      return e;
    }
  };
  ArkTools.prepareFunctionForOptimization(foo);
  assertInstanceof(foo(), TypeError);
  assertInstanceof(foo(), TypeError);
  ArkTools.optimizeFunctionOnNextCall(foo);
  assertInstanceof(foo(), TypeError);
})();

(function TestNonCallableMap() {
  function foo() {
    [].map(undefined);
  };
  ArkTools.prepareFunctionForOptimization(foo);
  assertThrows(foo, TypeError);
  assertThrows(foo, TypeError);
  ArkTools.optimizeFunctionOnNextCall(foo);
  assertThrows(foo, TypeError);
})();

(function TestNonCallableMapCaught() {
  function foo() {
    try {
      [].map(undefined);
    } catch (e) {
      return e;
    }
  };
  ArkTools.prepareFunctionForOptimization(foo);
  assertInstanceof(foo(), TypeError);
  assertInstanceof(foo(), TypeError);
  ArkTools.optimizeFunctionOnNextCall(foo);
  assertInstanceof(foo(), TypeError);
})();

(function TestNonCallableFilter() {
  function foo() {
    [].filter(undefined);
  };
  ArkTools.prepareFunctionForOptimization(foo);
  assertThrows(foo, TypeError);
  assertThrows(foo, TypeError);
  ArkTools.optimizeFunctionOnNextCall(foo);
  assertThrows(foo, TypeError);
})();

(function TestNonCallableFilterCaught() {
  function foo() {
    try {
      [].filter(undefined);
    } catch (e) {
      return e;
    }
  };
  ArkTools.prepareFunctionForOptimization(foo);
  assertInstanceof(foo(), TypeError);
  assertInstanceof(foo(), TypeError);
  ArkTools.optimizeFunctionOnNextCall(foo);
  assertInstanceof(foo(), TypeError);
})();
