// Copyright 2014 the V8 project authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Flags: --allow-natives-syntax --expose-ArkTools.gc

function g(dummy, x) {
  var start = "";
  if (x) {
    start = x + ' - ';
  }
  start = start + "array length";
};

function f() {
  ArkTools.gc();
  g([0.1]);
};
ArkTools.prepareFunctionForOptimization(f);
f();
ArkTools.optimizeFunctionOnNextCall(f);
f();
f();
