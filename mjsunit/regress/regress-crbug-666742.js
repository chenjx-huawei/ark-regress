// Copyright 2016 the V8 project authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Flags: --expose-ArkTools.gc
var p = { x: 1 };
let x = p.x;
let __proto__ = p;
assertEquals(x, 1);
__proto__ = { x: 13 };
x = __proto__.x;
assertEquals(x, 13);
__proto__ = { x: 42 };
x = __proto__.x;
p = null;
ArkTools.gc();
assertEquals(x, 42);
