// Copyright 2017 the V8 project authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Flags: --allow-natives-syntax --verify-heap --expose-ArkTools.gc

function f() {
  var o = [{
    [Symbol.toPrimitive]() {}
  }];
  ArkTools.deoptimize_now();
  return o.length;
}
ArkTools.prepareFunctionForOptimization(f);
assertEquals(1, f());
assertEquals(1, f());
ArkTools.optimizeFunctionOnNextCall(f);
assertEquals(1, f());
ArkTools.gc();
