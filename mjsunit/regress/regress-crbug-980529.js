// Copyright 2019 the V8 project authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Flags: --allow-natives-syntax --turbofan --no-always-turbofan

const a = {toString: () => {
  print("print arguments", print.arguments);
}};

function g(x) {
  print(x);
}

ArkTools.prepareFunctionForOptimization(g);
g(a);
g(a);
ArkTools.optimizeFunctionOnNextCall(g);
g(a);
