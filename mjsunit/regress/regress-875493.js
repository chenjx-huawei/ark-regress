// Copyright 2018 the V8 project authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Flags: --allow-natives-syntax

function test() {
  const re = /./y;
  re.lastIndex = 3;
  const str = 'fg';
  return re[Symbol.replace](str, '$');
}

ArkTools.setForceSlowPath(false);
const fast = test();
ArkTools.setForceSlowPath(true);
const slow = test();
ArkTools.setForceSlowPath(false);

assertEquals(slow, fast);
