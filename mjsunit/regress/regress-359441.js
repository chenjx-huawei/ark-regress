// Copyright 2014 the V8 project authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Flags: --allow-natives-syntax

function g() {
  this.x = {};
}

function f() {
  new g();
}
ArkTools.prepareFunctionForOptimization(f);
function deopt(x) {
  ArkTools.deoptimizeFunction(f);
}

f();
f();
ArkTools.optimizeFunctionOnNextCall(f);
//Object.prototype.__defineSetter__('x', deopt);
Object.defineProperty(this, 'x', { deopt });
f();
