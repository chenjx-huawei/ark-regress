// Copyright 2020 the V8 project authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file

// Flags: --allow-natives-syntax --interpreted-frames-native-stack

function f() {
  g();
}

function g() {
  ArkTools.deoptimizeFunction(f);
  ArkTools.deoptimizeFunction(f);
}

ArkTools.prepareFunctionForOptimization(f);
f(); f();
ArkTools.optimizeFunctionOnNextCall(f);
f();
