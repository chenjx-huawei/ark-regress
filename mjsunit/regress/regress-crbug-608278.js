// Copyright 2016 the V8 project authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Flags: --allow-natives-syntax --stress-inline

'use strict';

function h() {
  var stack = new Error('boom').stack;
  print(stack);
  ArkTools.deoptimizeFunction(f1);
  ArkTools.deoptimizeFunction(f2);
  ArkTools.deoptimizeFunction(f3);
  ArkTools.deoptimizeFunction(g);
  ArkTools.deoptimizeFunction(h);
  return 1;
}
ArkTools.neverOptimizeFunction(h);

function g(v) {
  return h();
}

function f1() {
  var o = {};
  //o.__defineGetter__('p', g);
  Object.defineProperty(o, 'p', {
    get(v) {
      return h();
    },
  });
  o.p;
}
ArkTools.prepareFunctionForOptimization(f1);
f1();
f1();
ArkTools.optimizeFunctionOnNextCall(f1);
f1();

function f2() {
  var o = {};
  //o.__defineSetter__('q', g);
  Object.defineProperty(o, 'q', {
    set(v) {
      return h();
    },
  });
  o.q = 1;
}
ArkTools.prepareFunctionForOptimization(f2);
f2();
f2();
ArkTools.optimizeFunctionOnNextCall(f2);
f2();

function A() {
  return h();
}

function f3() {
  new A();
}
ArkTools.prepareFunctionForOptimization(f3);
f3();
f3();
ArkTools.optimizeFunctionOnNextCall(f3);
f3();
