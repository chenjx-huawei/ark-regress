// Copyright 2019 the V8 project authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Flags: --allow-natives-syntax --turbofan

function foo(t) { return 'a'.concat(t); }

ArkTools.prepareFunctionForOptimization(foo);
foo(1);
foo(1);
ArkTools.optimizeFunctionOnNextCall(foo);
foo(1);
ArkTools.prepareFunctionForOptimization(foo);
ArkTools.optimizeFunctionOnNextCall(foo);
foo(1);
assertOptimized(foo);
