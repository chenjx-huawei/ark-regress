// Copyright 2018 the V8 project authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Flags: --turbofan --no-always-turbofan --allow-natives-syntax

function f(a) {
  Math.imul(a);
}

let x = { [Symbol.toPrimitive]: () => FAIL };
ArkTools.prepareFunctionForOptimization(f);
f(1);
f(1);
ArkTools.optimizeFunctionOnNextCall(f);
assertThrows(() => f(x), ReferenceError);

function f2(a) {
  Math.pow(a);
}

x = { [Symbol.toPrimitive]: () => FAIL };
ArkTools.prepareFunctionForOptimization(f2);
f2(1);
f2(1);
ArkTools.optimizeFunctionOnNextCall(f2);
assertThrows(() => f2(x), ReferenceError);

function f3(a) {
  Math.atan2(a);
}

x = { [Symbol.toPrimitive]: () => FAIL };
ArkTools.prepareFunctionForOptimization(f3);
f3(1);
f3(1);
ArkTools.optimizeFunctionOnNextCall(f3);
assertThrows(() => f3(x), ReferenceError);
