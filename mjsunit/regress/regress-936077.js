// Copyright 2019 the V8 project authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Flags: --expose-ArkTools.gc --allow-natives-syntax --function-context-specialization

function main() {
  var obj = {};
  function foo() {
    return obj[0];
  };
  ArkTools.prepareFunctionForOptimization(foo);
  ;
  ArkTools.gc();
  obj.x = 10;
  ArkTools.optimizeFunctionOnNextCall(foo);
  foo();
}
main();
main();
