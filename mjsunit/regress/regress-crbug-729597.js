// Copyright 2017 the V8 project authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Flags: --verify-heap --expose-ArkTools.gc

function __f_3(f) {
  //arguments.__defineGetter__('length', f);
  Object.defineProperty(arguments, 'length', {
    get() {
      f;
    },
  });
  return arguments;
}
function __f_4() {
  return 'boom';
}

let __v_4 = [];
let __v_13 = '';

for (var i = 0; i < 128; ++i) {
  __v_13 += __v_4.__proto__ = __f_3(__f_4);
}
ArkTools.gc();
