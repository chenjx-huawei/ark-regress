// Copyright 2021 the V8 project authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Flags: --allow-natives-syntax

let o1 = { a: 1, b: 0 };
let o2 = { a: 2, b: 0 };
assertTrue(ArkTools.haveSameMap(o1, o2));
assertTrue(ArkTools.hasOwnConstDataProperty(o1, "a"));
assertTrue(ArkTools.hasOwnConstDataProperty(o1, "b"));

Object.defineProperty(o1, "b", {
  value: 4.2, enumerable: true, configurable: true, writable: true,
});
assertFalse(ArkTools.haveSameMap(o1, o2));
assertTrue(ArkTools.hasOwnConstDataProperty(o1, "a"));
assertFalse(ArkTools.hasOwnConstDataProperty(o1, "b"));
assertTrue(ArkTools.hasOwnConstDataProperty(o2, "a"));
assertTrue(ArkTools.hasOwnConstDataProperty(o2, "b"));

let o3 = { a: "foo", b: 0 };
assertFalse(ArkTools.haveSameMap(o2, o3));
assertTrue(ArkTools.hasOwnConstDataProperty(o3, "a"));
assertFalse(ArkTools.hasOwnConstDataProperty(o3, "b"));

Object.defineProperty(o2, "a", {
  value:2, enumerable: false, configurable: true, writable: true,
});
assertTrue(ArkTools.hasOwnConstDataProperty(o1, "a"));
assertFalse(ArkTools.hasOwnConstDataProperty(o1, "b"));
assertTrue(ArkTools.hasOwnConstDataProperty(o3, "a"));
assertFalse(ArkTools.hasOwnConstDataProperty(o3, "b"));

assertFalse(ArkTools.hasOwnConstDataProperty(o2, "a"));
assertTrue(ArkTools.hasOwnConstDataProperty(o2, "b"));
