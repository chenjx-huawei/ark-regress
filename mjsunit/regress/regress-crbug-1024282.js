// Copyright 2019 the V8 project authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Flags: --allow-natives-syntax --expose-ArkTools.gc

assertThrows = function assertThrows(code) {};
ArkTools.prepareFunctionForOptimization(assertThrows);

function foo(val) {
  var arr = [];
  function bar() {
    function m1() {}
    ArkTools.prepareFunctionForOptimization(m1);
    assertThrows(m1);
    0 in arr;
  }
  ArkTools.prepareFunctionForOptimization(bar);
  bar();  // virtual context distance of 1 from native_context
  ArkTools.gc();
  bar(true);
}

ArkTools.prepareFunctionForOptimization(foo);
foo();
foo();
ArkTools.optimizeFunctionOnNextCall(foo);
foo();
