// Copyright 2018 the V8 project authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Flags: --allow-natives-syntax --expose-ArkTools.gc --verify-heap

function f() {
  let C = class {};
  for (var i = 0; i < 5; ++i) {
    ArkTools.gc();
    if (i == 2) ArkTools.optimizeOsr();
    C.prototype.foo = i + 9000000000000000;
  }
}
ArkTools.prepareFunctionForOptimization(f);
f();
