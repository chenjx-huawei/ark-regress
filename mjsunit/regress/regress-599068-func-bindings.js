// Copyright 2016 the V8 project authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Flags: --allow-natives-syntax

// Tests function bindings are correctly handled in ignition.
"use strict";

(function f() {
  function assignStrict() {
    f = 0;
  }
  assertThrows(assignStrict, TypeError);

  function assignStrictLookup() {
    eval("f = 1;");
  }
  assertThrows(assignStrictLookup, TypeError);
})();

// Tests for compound assignments which are handled differently
// in crankshaft.
(function f() {
  function assignStrict() {
    f += "x";
  };
  ArkTools.prepareFunctionForOptimization(assignStrict);
  assertThrows(assignStrict, TypeError);
  assertThrows(assignStrict, TypeError);
  ArkTools.optimizeFunctionOnNextCall(assignStrict);
  assertThrows(assignStrict, TypeError);
})();
