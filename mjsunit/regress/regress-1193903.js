// Copyright 2014 the V8 project authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.
//
// Flags: --allow-natives-syntax

var s_uninternalized = "concurrent" + "-skip-finalization";
ArkTools.internalizeString(s_uninternalized);

function foo() {}

// Make sure ArkTools.optimizeFunctionOnNextCall works with a non-internalized
// string parameter.
ArkTools.prepareFunctionForOptimization(foo);
ArkTools.optimizeFunctionOnNextCall(foo, s_uninternalized)
