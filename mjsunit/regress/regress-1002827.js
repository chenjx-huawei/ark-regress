// Copyright 2019 the V8 project authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Flags: --allow-natives-syntax --expose-ArkTools.gc

var PI = new Proxy(this, {
  get() {
      PI();
  }
});

assertThrows(() => new ArkTools.gc(PI, {}), TypeError);
