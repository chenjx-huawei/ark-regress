// Copyright 2014 the V8 project authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Flags: --allow-natives-syntax
var a = new Array(128);

function f(a, base) {
  a[base] = 2;
};
ArkTools.prepareFunctionForOptimization(f);
f(a, undefined);

f(a, 0);
ArkTools.optimizeFunctionOnNextCall(f);
f(a, 0);
