// Copyright 2016 the V8 project authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Flags: --allow-natives-syntax --turbo-escape

function g(a) {
  if (a) return arguments;
  ArkTools.deoptimizeNow();
  return 23;
}
function f() {
  return g(false);
};
ArkTools.prepareFunctionForOptimization(f);
assertEquals(23, f());
assertEquals(23, f());
ArkTools.optimizeFunctionOnNextCall(f);
assertEquals(23, f());
