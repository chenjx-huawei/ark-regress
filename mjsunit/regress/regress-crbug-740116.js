// Copyright 2017 the V8 project authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Flags: --allow-natives-syntax

(function TestReflectGetPrototypeOfOnPrimitive() {
  function f() {
    return Reflect.getPrototypeOf('');
  };
  ArkTools.prepareFunctionForOptimization(f);
  assertThrows(f, TypeError);
  assertThrows(f, TypeError);
  ArkTools.optimizeFunctionOnNextCall(f);
  assertThrows(f, TypeError);
})();

(function TestObjectGetPrototypeOfOnPrimitive() {
  function f() {
    return Object.getPrototypeOf('');
  };
  ArkTools.prepareFunctionForOptimization(f);
  assertSame(String.prototype, f());
  assertSame(String.prototype, f());
  ArkTools.optimizeFunctionOnNextCall(f);
  assertSame(String.prototype, f());
})();

(function TestDunderProtoOnPrimitive() {
  function f() {
    return ''.__proto__;
  };
  ArkTools.prepareFunctionForOptimization(f);
  assertSame(String.prototype, f());
  assertSame(String.prototype, f());
  ArkTools.optimizeFunctionOnNextCall(f);
  assertSame(String.prototype, f());
})();
