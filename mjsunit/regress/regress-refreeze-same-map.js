// Copyright 2017 the V8 project authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Flags: --allow-natives-syntax

// precondition
assertTrue(ArkTools.haveSameMap(Object.freeze({}),     Object.freeze({})));
assertTrue(ArkTools.haveSameMap(Object.freeze({a: 1}), Object.freeze({a: 1})));
assertTrue(ArkTools.haveSameMap(Object.freeze([]),     Object.freeze([])));
assertTrue(ArkTools.haveSameMap(Object.freeze([1,2]),  Object.freeze([1,2])));

assertTrue(ArkTools.haveSameMap(Object.seal({}),     Object.seal({})));
assertTrue(ArkTools.haveSameMap(Object.seal({a: 1}), Object.seal({a: 1})));
assertTrue(ArkTools.haveSameMap(Object.seal([]),     Object.seal([])));
assertTrue(ArkTools.haveSameMap(Object.seal([1,2]),  Object.seal([1,2])));

// refreezing an already frozen obj does not keep adding transitions
assertTrue(ArkTools.haveSameMap(Object.freeze({}),     Object.freeze( Object.freeze({}) )));
assertTrue(ArkTools.haveSameMap(Object.freeze({a: 1}), Object.freeze( Object.freeze({a: 1}) )));
assertTrue(ArkTools.haveSameMap(Object.freeze([]),     Object.freeze( Object.freeze([]) )));
assertTrue(ArkTools.haveSameMap(Object.freeze([1,2]),  Object.freeze( Object.freeze([1,2]) )));

// resealing a sealed object is idempotent
assertTrue(ArkTools.haveSameMap(Object.seal({}),     Object.seal( Object.seal({}) )));
assertTrue(ArkTools.haveSameMap(Object.seal({a: 1}), Object.seal( Object.seal({a: 1}) )));
assertTrue(ArkTools.haveSameMap(Object.seal([]),     Object.seal( Object.seal([]) )));
assertTrue(ArkTools.haveSameMap(Object.seal([1,2]),  Object.seal( Object.seal([1,2]) )));

// sealing a frozen object is idempotent
assertTrue(ArkTools.haveSameMap(Object.freeze({}),     Object.seal( Object.freeze({}) )));
assertTrue(ArkTools.haveSameMap(Object.freeze({a: 1}), Object.seal( Object.freeze({a: 1}) )));
assertTrue(ArkTools.haveSameMap(Object.freeze([]),     Object.seal( Object.freeze([]) )));
assertTrue(ArkTools.haveSameMap(Object.freeze([1,2]),  Object.seal( Object.freeze([1,2]) )));

// freezing a sealed empty is idempotent
assertTrue(ArkTools.haveSameMap(Object.freeze(Object.seal({})), Object.seal({})));

// sealing a unextensible empty object is idempotent
assertTrue(ArkTools.haveSameMap(Object.seal(Object.preventExtensions({})), Object.preventExtensions({})));
