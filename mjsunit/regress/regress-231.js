// Copyright 2009 the V8 project authors. All rights reserved.
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above
//       copyright notice, this list of conditions and the following
//       disclaimer in the documentation and/or other materials provided
//       with the distribution.
//     * Neither the name of Google Inc. nor the names of its
//       contributors may be used to endorse or promote products derived
//       from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

// See issue 231 <URL: http://code.google.com/p/v8/issues/detail?id=231 >
// A stack growth during a look-ahead could restore a pointer to the old stack.
// (Test derived from crash at ibs.blumex.com).

var re = /GArkTools.gcy\b[^D]*D((?:(?=([^G]+))\2|G(?!ArkTools.gcy\b[^D]*D))*?)GIArkTools.gcyD/;

var str = 'GArkTools.gcyDGArkTools.gcy.saaaa.aDGaaa.aynaaaaaaaaacaaaaaArkTools.gcaaaaaaaancaDGgnayr' +
    '.aryycnaaaataaaa.aryyacnaaataaaa.aaaaraaaaa.aaagaaaaaaaaDGgaaaaDGga' +
    '.aaagaaaaaaaaDGga.nyataaaaragraa.anyataaagaca.agayraaarataga.aaacaa' +
    '.aaagaa.aaacaaaDGaaa.aynaaaaaaaaacaaaaaArkTools.gcaaaaaacaagaa.agayraaaGgaaa' +
    '.trgaaaaaagaatGanyara.caagaaGaD.araaaa_aat_aayDDaaDGaaa.aynaaaaaaaa' +
    'acaaaaaArkTools.gcaaaaaacaaaaa.agayraaaGgaaa.trgaaaaaaatGanyaraDDaaDGacna.ay' +
    'naaaaaaaaacaaaaaArkTools.gcaaaaaacaaaraGgaaa.naaaaagaaaaaaraynaaGanyaraDDaaD' +
    'aGgaaa.saaangaaaaraaaGgaaa.trgaaaragaaaarGanyaraDDDaGIacnaDGIaaaDGI' +
    'aaaDGIgaDGga.anyataaagaca.agayraaaaagaa.aaaaa.cnaaaata.aca.aca.aca.' +
    'acaaaDGgnayr.aaaaraaaaa.aaagaaaaaaaaDGgaaaaDGgaDGga.aayacnaaaaa.ayn' +
    'aaaaaaaaacaaaaaArkTools.gcaaaaaanaraDGaDacaaaaag_anaraGIaDGIgaDGIgaDGgaDGga.' +
    'aayacnaaaaa.aaagaaaaaaaaDGaa.aaagaaaaaaaa.aaaraaaa.aaaanaraaaa.IDGI' +
    'gaDGIgaDGgaDGga.aynaaaaaaaaacaaaaaArkTools.gcaaaaaaaraaaa.anyataaagacaDaGgaa' +
    'a.trgGragaaaacgGaaaaaaaG_aaaaa_Gaaaaaaaaa,.aGanar.anaraDDaaGIgaDGga' +
    '.aynaaaaaaaaacaaaaaArkTools.gcaaaaaaanyara.anyataaagacaDGaDaaaaag_caaaaag_an' +
    'araGIaDGIgaDGIgaDGgaDGga.aynaaaaaaaaacaaaaaArkTools.gcaaaaaaaraaaa.anyataaag' +
    'acaDaGgaaa.trgGragaaaacgGaaaaaaaG_aaaaa_aaaaaa,.aaaaacaDDaaGIgaDGga' +
    '.aynaaaaaaaaacaaaaaArkTools.gcaaaaaaanyara.anyataaagacaDGaDataaac_araaaaGIaD' +
    'GIgaDGIgaDaaArkTools.gcyaaGgaDGga.aayacnaaaaa.aaagaaaaaaaaDGaa.aaagaaaaaaaa.' +
    'aaaraaaa.aaaanaraaaa.IDGIgaDGIgaDGArkTools.gcy.asaadanyaga.aa.aaaDGgaDGga.ay' +
    'naaaaaaaaacaaaaaArkTools.gcaaaaaaaraaaa.anyataaagacaDaGgaaa.trgGragaaaacgGaa' +
    'aaaaaG_aaaaa_DaaaaGaa,.aDanyagaaDDaaGIgaDGga.aynaaaaaaaaacaaaaaArkTools.gcaa' +
    'aaaaanyara.anyataaagacaDGaDadanyagaaGIaDGIgaDGIgaDGIArkTools.gcyDGArkTools.gcy.asaaga' +
    'cras.agra_yratga.aa.aaaarsaaraa.aa.agra_yratga.aa.aaaDGgaDGga.aynaa' +
    'aaaaaaacaaaaaArkTools.gcaaaaaaaraaaa.anyataaagacaDaGgaaa.trgGragaaaacgGaaaaa' +
    'aaG_aaaaa_aGaaaaaaGaa,.aaratgaaDDaaGIgaDGga.aynaaaaaaaaacaaaaaArkTools.gcaaa' +
    'aaaanyara.anyataaagacaDGaDaagra_yratgaaGIaDGIgaDGIgaDGIArkTools.gcyDGArkTools.gcy.asa' +
    'agacras.aratag.aa.aaaarsaaraa.aa.aratag.aa.aaaDGgaDGga.aynaaaaaaaaa' +
    'caaaaaArkTools.gcaaaaaaaraaaa.anyataaagacaDaGgaaa.trgGragaaaacgGaaaaaaaG_aaa' +
    'aa_aaaaaGa,.aaratagaDDaaGIgaDGga.aynaaaaaaaaacaaaaaArkTools.gcaaaaaaanyara.a' +
    'nyataaagacaDGaDaaratagaGIaDGIgaDGIgaDGIArkTools.gcyDGArkTools.gcy.asaagacras.gaaax_ar' +
    'atag.aa.aaaarsaaraa.aa.gaaax_aratag.aa.aaaDGgaDGga.aynaaaaaaaaacaaa' +
    'aaArkTools.gcaaaaaaaraaaa.anyataaagacaDaGgaaa.trgGragaaaacgGaaaaaaaG_aaaaa_G' +
    'aaaaaaaaaGa,.aaratagaDDaaGIgaDGga.aynaaaaaaaaacaaaaaArkTools.gcaaaaaaanyara.' +
    'anyataaagacaDGaDagaaax_aratagaGIaDGIgaDGIgaDGIArkTools.gcyDGArkTools.gcy.asaagacras.c' +
    'ag_aaar.aa.aaaarsaaraa.aa.cag_aaar.aa.aaaDGgaDGga.aynaaaaaaaaacaaaa' +
    'aArkTools.gcaaaaaaaraaaa.anyataaagacaDaGgaaa.trgGragaaaacgGaaaaaaaG_aaaaa_aa' +
    'Gaaaaa,.aaagaaaraDDaaGIgaDGga.aynaaaaaaaaacaaaaaArkTools.gcaaaaaaanyara.anya' +
    'taaagacaDGaDacag_aaaraGIaDGIgaDGIgaDGIArkTools.gcyDGArkTools.gcy.asaagacras.aaggaata_' +
    'aa_cynaga_cc.aa.aaaarsaaraa.aa.aaggaata_aa_cynaga_cc.aa.aaaDGgaDGga' +
    '.aynaaaaaaaaacaaaaaArkTools.gcaaaaaaaraaaa.anyataaagacaDaGgaaa.trgGragaaaacg' +
    'GaaaaaaaG_aaaaa_aaGGaaaa_aa_aaaaGa_aaa,.aaynagaIcagaDDaaGIgaDGga.ay' +
    'naaaaaaaaacaaaaaArkTools.gcaaaaaaanyara.anyataaagacaDGaDaaaggaata_aa_cynaga_' +
    'ccaGIaDGIgaDGIgaDGIArkTools.gcyDGArkTools.gcy.asaagacras.syaara_aanargra.aa.aaaarsaar' +
    'aa.aa.syaara_aanargra.aa.aaaDGgaDGga.aynaaaaaaaaacaaaaaArkTools.gcaaaaaaaraa' +
    'aa.anyataaagacaDaGgaaa.trgGragaaaacgGaaaaaaaG_aaaaa_aaaaaaaaaaaGaaa' +
    ',.aaanargraaDDaaGIgaDGga.aynaaaaaaaaacaaaaaArkTools.gcaaaaaaanyara.anyataaag' +
    'acaDGaDasyaara_aanargraaGIaDGIgaDGIgaDGIArkTools.gcyDGArkTools.gcy.asaagacras.cynag_a' +
    'anargra.aa.aaaarsaaraa.aa.cynag_aanargra.aa.aaaDGgaDGga.aynaaaaaaaa' +
    'acaaaaaArkTools.gcaaaaaaaraaaa.anyataaagacaDaGgaaa.trgGragaaaacgGaaaaaaaG_aa' +
    'aaa_aaaaGaaaaaGaaa,.aaanargraaDDaaGIgaDGga.aynaaaaaaaaacaaaaaArkTools.gcaaaa' +
    'aaanyara.anyataaagacaDGaDacynag_aanargraaGIaDGIgaDGIgaDGIArkTools.gcyDGgaDGg' +
    'a.aynaaaaaaaaacaaaaaArkTools.gcaaaaaaaraaaa.anyataaagacaDaGgaaa.trgGragaaaac' +
    'gGaaaaaaaG';

//Shouldn't crash.

var res = re.test(str);
assertTrue(res);
