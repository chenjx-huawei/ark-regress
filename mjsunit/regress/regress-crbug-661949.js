// Copyright 2016 the V8 project authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Flags: --allow-natives-syntax

var foo = function() {
  'use asm';
  function foo() {
    ''[0];
  };
  ArkTools.prepareFunctionForOptimization(foo);
  return foo;
}();

foo();
ArkTools.optimizeFunctionOnNextCall(foo);
foo();
