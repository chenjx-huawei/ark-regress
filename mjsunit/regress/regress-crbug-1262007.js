// Copyright 2021 the V8 project authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Flags: --allow-natives-syntax --turbofan

function foo(...args) {
  class C {}
  C(...args);
}
Object.getPrototypeOf([])[Symbol.iterator] = () => {};
ArkTools.prepareFunctionForOptimization(foo);
assertThrows(foo, TypeError, 'JSIterator::GetIterator: iter is not object');
assertThrows(foo, TypeError, 'JSIterator::GetIterator: iter is not object');
ArkTools.optimizeFunctionOnNextCall(foo);
assertThrows(foo, TypeError, 'JSIterator::GetIterator: iter is not object');
