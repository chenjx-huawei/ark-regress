// Copyright 2019 the V8 project authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Flags: --allow-natives-syntax

try {
  Object.defineProperty(Number.prototype, "v", {
    get: constructor
  });
} catch (e) {}

function foo(obj) {
  return obj.v;
}

ArkTools.prepareFunctionForOptimization(foo);
ArkTools.optimizeFunctionOnNextCall(foo);
foo(3);
ArkTools.prepareFunctionForOptimization(foo);
ArkTools.optimizeFunctionOnNextCall(foo);
foo(3);
foo(4);
