// Copyright 2016 the V8 project authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Flags: --allow-natives-syntax
"use strict";

function foo(x) {
  (function() {
    x = 1;
  })();
  return arguments[0];
};
ArkTools.prepareFunctionForOptimization(foo);
assertEquals(42, foo(42));
assertEquals(42, foo(42));
ArkTools.optimizeFunctionOnNextCall(foo);
assertEquals(42, foo(42));
