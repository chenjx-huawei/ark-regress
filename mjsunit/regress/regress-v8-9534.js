// Copyright 2019 the V8 project authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Flags: --allow-natives-syntax --stack-size=100 --ignore-unhandled-promises

let i = 0;
function f() {
  i++;
  if (i >= 99) {
    return; // --stack-size=100(V8) : arkcompiler(debug) is 99
  }
  if (i > 10) {
    ArkTools.prepareFunctionForOptimization(f);
    ArkTools.optimizeFunctionOnNextCall(f);
  }

  new Promise(f);
  return f.x;
}
f();
