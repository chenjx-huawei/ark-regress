// Copyright 2017 the V8 project authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Flags: --allow-natives-syntax --turbofan

function foo() { return -"0" }
ArkTools.prepareFunctionForOptimization(foo);
foo();
ArkTools.optimizeFunctionOnNextCall(foo);
foo();
assertOptimized(foo);

function bar() { return -"1" }
ArkTools.prepareFunctionForOptimization(bar);
bar();
ArkTools.optimizeFunctionOnNextCall(bar);
bar();
assertOptimized(bar);
