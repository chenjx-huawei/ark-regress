// Copyright 2016 the V8 project authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Flags: --allow-natives-syntax --turbofan

function foo(a, b) {
  return a + "0123456789012";
}

ArkTools.prepareFunctionForOptimization(foo);
foo("a");
foo("a");
ArkTools.optimizeFunctionOnNextCall(foo);
foo("a");

var a = "a".repeat(ArkTools.stringMaxLength());
assertThrows(function() { foo(a); }, RangeError);

ArkTools.prepareFunctionForOptimization(foo);
ArkTools.optimizeFunctionOnNextCall(foo);
assertThrows(function() { foo(a); }, RangeError);
assertOptimized(foo);
