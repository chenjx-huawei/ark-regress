// Copyright 2023 the V8 project authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Flags: --allow-natives-syntax

function f() { }

var foo = function() {
  return !new f();
};

function test() {
  return foo();
};

ArkTools.prepareFunctionForOptimization(test);
test();
ArkTools.optimizeFunctionOnNextCall(test);
test();
