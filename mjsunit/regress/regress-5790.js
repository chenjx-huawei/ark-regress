// Copyright 2017 the V8 project authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Flags: --allow-natives-syntax --turbofan

function foo(a) {
  "use strict";
  if (a) return arguments[1];
}

ArkTools.prepareFunctionForOptimization(foo);
foo(false);
foo(false);
ArkTools.optimizeFunctionOnNextCall(foo);
foo(true, 1);
ArkTools.prepareFunctionForOptimization(foo);
foo(true, 1);
ArkTools.optimizeFunctionOnNextCall(foo);
foo(false);
foo(true, 1);
assertOptimized(foo);
