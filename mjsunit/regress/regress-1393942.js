// Copyright 2022 the V8 project authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.
//
// Flags: --harmony-rab-gsab --allow-natives-syntax

const gsab = new SharedArrayBuffer(4,{"maxByteLength":8});
const u16arr = new Uint16Array(gsab);

function foo(obj) {
    obj[1] = 0;
}

function test() {
    const u32arr = new Uint32Array();
    foo(u32arr);
    foo(u16arr);
}

ArkTools.prepareFunctionForOptimization(test);
ArkTools.prepareFunctionForOptimization(foo);
test();
ArkTools.optimizeFunctionOnNextCall(foo);
test();
ArkTools.optimizeFunctionOnNextCall(test);
test();
