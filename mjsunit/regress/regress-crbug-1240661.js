// Copyright 2021 the V8 project authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Flags: --gdbjit --allow-natives-syntax

let f = new Function("boom");

ArkTools.prepareFunctionForOptimization(f);
ArkTools.optimizeFunctionOnNextCall(f);
assertThrows(f, ReferenceError);
