// Copyright 2014 the V8 project authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Flags: --allow-natives-syntax

function f(a, b) {
  return b + a.x++;
}
ArkTools.prepareFunctionForOptimization(f);
var o = {};
// o.__defineGetter__('x', function () {
//   return 1;
// });

Object.defineProperty(o, 'x', {
  get() {
    return 1;
  },
  set(value) {
    this._x = value;
  },
});
assertEquals(4, f(o, 3));
assertEquals(4, f(o, 3));
ArkTools.optimizeFunctionOnNextCall(f);
assertEquals(4, f(o, 3));
