// Copyright 2022 the V8 project authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Flags: --harmony-rab-gsab

for (let i = 0; i < 1000; i++) {
  const rab = new ArrayBuffer(1632, {"maxByteLength": 4096});
  const ta1 = new Uint32Array(rab);
  const ta2 = new Float32Array(rab);
  ta1["set"](ta2);
}
