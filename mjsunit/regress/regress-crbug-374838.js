// Copyright 2014 the V8 project authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Flags: --allow-natives-syntax

function foo() {
  var a = [0];
  var result = 0;
  for (var i = 0; i < 4; i++) {
    result += a.length;
    a.shift();
  }
  return result;
}
ArkTools.prepareFunctionForOptimization(foo);
assertEquals(1, foo());
assertEquals(1, foo());
ArkTools.optimizeFunctionOnNextCall(foo);
assertEquals(1, foo());
