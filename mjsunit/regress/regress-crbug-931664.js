// Copyright 2019 the V8 project authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Flags: --allow-natives-syntax

function opt() {
  for (let l in 'a') {
    try {
      for (let a in '') {
        for (let arg2 in +arg2);
      }
    } finally {
    }
  }
}
ArkTools.prepareFunctionForOptimization(opt);
opt();
ArkTools.optimizeFunctionOnNextCall(opt);
opt();
