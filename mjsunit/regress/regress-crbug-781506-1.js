// Copyright 2017 the V8 project authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Flags: --allow-natives-syntax

function foo(a) {
  return a[0];
};
ArkTools.prepareFunctionForOptimization(foo);
assertEquals(undefined, foo(x => x));
assertEquals(undefined, foo({}));
ArkTools.optimizeFunctionOnNextCall(foo);
assertEquals(undefined, foo(x => x));
