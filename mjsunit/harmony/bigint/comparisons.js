// Copyright 2017 the V8 project authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Flags: --allow-natives-syntax

'use strict'

const minus_one = BigInt(-1);
const zero = BigInt(0);
const another_zero = BigInt(0);
const one = BigInt(1);
const another_one = BigInt(1);
const two = BigInt(2);
const three = BigInt(3);
const six = BigInt(6);


// Strict equality
{
  assertTrue(zero === zero);
  assertFalse(zero !== zero);

  assertTrue(zero === another_zero);
  assertFalse(zero !== another_zero);

  assertFalse(zero === one);
  assertTrue(zero !== one);
  assertTrue(one !== zero);
  assertFalse(one === zero);

  assertFalse(zero === 0);
  assertTrue(zero !== 0);
  assertFalse(0 === zero);
  assertTrue(0 !== zero);
}{
  assertTrue(ArkTools.strictEqual(zero, zero));
  assertFalse(ArkTools.strictNotEqual(zero, zero));

  assertTrue(ArkTools.strictEqual(zero, another_zero));
  assertFalse(ArkTools.strictNotEqual(zero, another_zero));

  assertFalse(ArkTools.strictEqual(zero, one));
  assertTrue(ArkTools.strictNotEqual(zero, one));
  assertTrue(ArkTools.strictNotEqual(one, zero));
  assertFalse(ArkTools.strictEqual(one, zero));

  assertFalse(ArkTools.strictEqual(zero, 0));
  assertTrue(ArkTools.strictNotEqual(zero, 0));
  assertFalse(ArkTools.strictEqual(0, zero));
  assertTrue(ArkTools.strictNotEqual(0, zero));
}

// Abstract equality
{
  assertTrue(ArkTools.equal(zero, zero));
  assertTrue(ArkTools.equal(zero, another_zero));
  assertFalse(ArkTools.equal(zero, one));
  assertFalse(ArkTools.equal(one, zero));

  assertTrue(ArkTools.equal(zero, +0));
  assertTrue(ArkTools.equal(zero, -0));
  assertTrue(ArkTools.equal(+0, zero));
  assertTrue(ArkTools.equal(-0, zero));

  assertTrue(ArkTools.equal(zero, false));
  assertTrue(ArkTools.equal(one, true));
  assertFalse(ArkTools.equal(zero, true));
  assertFalse(ArkTools.equal(one, false));
  assertTrue(ArkTools.equal(false, zero));
  assertTrue(ArkTools.equal(true, one));
  assertFalse(ArkTools.equal(true, zero));
  assertFalse(ArkTools.equal(false, one));

  assertTrue(ArkTools.equal(one, 1));
  assertTrue(ArkTools.equal(one, Number(1)));
  assertTrue(ArkTools.equal(1, one));
  assertTrue(ArkTools.equal(Number(1), one));

  assertTrue(ArkTools.equal(minus_one, -1));
  assertTrue(ArkTools.equal(minus_one, Number(-1)));
  assertTrue(ArkTools.equal(-1, minus_one));
  assertTrue(ArkTools.equal(Number(-1), minus_one));

  assertFalse(ArkTools.equal(one, -1));
  assertFalse(ArkTools.equal(one, Number(-1)));
  assertFalse(ArkTools.equal(-1, one));
  assertFalse(ArkTools.equal(Number(-1), one));

  assertFalse(ArkTools.equal(one, 1.0000000000001));
  assertFalse(ArkTools.equal(1.0000000000001, one));

  assertTrue(ArkTools.equal(zero, ""));
  assertTrue(ArkTools.equal("", zero));
  assertTrue(ArkTools.equal(one, "1"));
  assertTrue(ArkTools.equal("1", one));
  assertFalse(ArkTools.equal(one, "a"));
  assertFalse(ArkTools.equal("a", one));

  assertTrue(ArkTools.equal(one, {valueOf() { return true }}));
  assertTrue(ArkTools.equal({valueOf() { return true }}, one));
  assertFalse(ArkTools.equal(two, {valueOf() { return true }}));
  assertFalse(ArkTools.equal({valueOf() { return true }}, two));

  assertFalse(ArkTools.equal(Symbol(), zero));
  assertFalse(ArkTools.equal(zero, Symbol()));
}{
  assertTrue(zero == zero);
  assertTrue(zero == another_zero);
  assertFalse(zero == one);
  assertFalse(one == zero);

  assertTrue(zero == +0);
  assertTrue(zero == -0);
  assertTrue(+0 == zero);
  assertTrue(-0 == zero);

  assertTrue(zero == false);
  assertTrue(one == true);
  assertFalse(zero == true);
  assertFalse(one == false);
  assertTrue(false == zero);
  assertTrue(true == one);
  assertFalse(true == zero);
  assertFalse(false == one);

  assertTrue(one == 1);
  assertTrue(one == Number(1));
  assertTrue(1 == one);
  assertTrue(Number(1) == one);

  assertTrue(minus_one == -1);
  assertTrue(minus_one == Number(-1));
  assertTrue(-1 == minus_one);
  assertTrue(Number(-1) == minus_one);

  assertFalse(one == -1);
  assertFalse(one == Number(-1));
  assertFalse(-1 == one);
  assertFalse(Number(-1) == one);

  assertFalse(one == 1.0000000000001);
  assertFalse(1.0000000000001 == one);

  assertTrue(zero == "");
  assertTrue("" == zero);
  assertTrue(zero == " \t\r\n");
  assertTrue(one == "1");
  assertTrue("1" == one);
  assertFalse(" \t\r\n" == one);
  assertFalse(one == "a");
  assertFalse("a" == one);

  assertTrue(zero == "00000000000000" + "0");

  assertTrue(one == {valueOf() { return true }});
  assertTrue({valueOf() { return true }} == one);
  assertFalse(two == {valueOf() { return true }});
  assertFalse({valueOf() { return true }} == two);

  assertFalse(Symbol() == zero);
  assertFalse(zero == Symbol());

  assertTrue(one == "0b1");
  assertTrue(" 0b1" == one);
  assertTrue(one == "0o1");
  assertTrue("0o1 " == one);
  assertTrue(one == "\n0x1");
  assertTrue("0x1" == one);

  assertFalse(one == "1j");
  assertFalse(one == "0b1ju");
  assertFalse(one == "0o1jun");
  assertFalse(one == "0x1junk");
}{
  assertFalse(ArkTools.notEqual(zero, zero));
  assertFalse(ArkTools.notEqual(zero, another_zero));
  assertTrue(ArkTools.notEqual(zero, one));
  assertTrue(ArkTools.notEqual(one, zero));

  assertFalse(ArkTools.notEqual(zero, +0));
  assertFalse(ArkTools.notEqual(zero, -0));
  assertFalse(ArkTools.notEqual(+0, zero));
  assertFalse(ArkTools.notEqual(-0, zero));

  assertFalse(ArkTools.notEqual(zero, false));
  assertFalse(ArkTools.notEqual(one, true));
  assertTrue(ArkTools.notEqual(zero, true));
  assertTrue(ArkTools.notEqual(one, false));
  assertFalse(ArkTools.notEqual(false, zero));
  assertFalse(ArkTools.notEqual(true, one));
  assertTrue(ArkTools.notEqual(true, zero));
  assertTrue(ArkTools.notEqual(false, one));

  assertFalse(ArkTools.notEqual(one, 1));
  assertFalse(ArkTools.notEqual(one, Number(1)));
  assertFalse(ArkTools.notEqual(1, one));
  assertFalse(ArkTools.notEqual(Number(1), one));

  assertFalse(ArkTools.notEqual(minus_one, -1));
  assertFalse(ArkTools.notEqual(minus_one, Number(-1)));
  assertFalse(ArkTools.notEqual(-1, minus_one));
  assertFalse(ArkTools.notEqual(Number(-1), minus_one));

  assertTrue(ArkTools.notEqual(one, -1));
  assertTrue(ArkTools.notEqual(one, Number(-1)));
  assertTrue(ArkTools.notEqual(-1, one));
  assertTrue(ArkTools.notEqual(Number(-1), one));

  assertTrue(ArkTools.notEqual(one, 1.0000000000001));
  assertTrue(ArkTools.notEqual(1.0000000000001, one));

  assertFalse(ArkTools.notEqual(zero, ""));
  assertFalse(ArkTools.notEqual("", zero));
  assertFalse(ArkTools.notEqual(one, "1"));
  assertFalse(ArkTools.notEqual("1", one));
  assertTrue(ArkTools.notEqual(one, "a"));
  assertTrue(ArkTools.notEqual("a", one));

  assertFalse(ArkTools.notEqual(one, {valueOf() { return true }}));
  assertFalse(ArkTools.notEqual({valueOf() { return true }}, one));
  assertTrue(ArkTools.notEqual(two, {valueOf() { return true }}));
  assertTrue(ArkTools.notEqual({valueOf() { return true }}, two));

  assertTrue(ArkTools.notEqual(Symbol(), zero));
  assertTrue(ArkTools.notEqual(zero, Symbol()));
}{
  assertFalse(zero != zero);
  assertFalse(zero != another_zero);
  assertTrue(zero != one);
  assertTrue(one != zero);

  assertFalse(zero != +0);
  assertFalse(zero != -0);
  assertFalse(+0 != zero);
  assertFalse(-0 != zero);

  assertFalse(zero != false);
  assertFalse(one != true);
  assertTrue(zero != true);
  assertTrue(one != false);
  assertFalse(false != zero);
  assertFalse(true != one);
  assertTrue(true != zero);
  assertTrue(false != one);

  assertFalse(one != 1);
  assertFalse(one != Number(1));
  assertFalse(1 != one);
  assertFalse(Number(1) != one);

  assertFalse(minus_one != -1);
  assertFalse(minus_one != Number(-1));
  assertFalse(-1 != minus_one);
  assertFalse(Number(-1) != minus_one);

  assertTrue(one != -1);
  assertTrue(one != Number(-1));
  assertTrue(-1 != one);
  assertTrue(Number(-1) != one);

  assertTrue(one != 1.0000000000001);
  assertTrue(1.0000000000001 != one);

  assertFalse(zero != "");
  assertFalse("" != zero);
  assertFalse(one != "1");
  assertFalse("1" != one);
  assertTrue(one != "a");
  assertTrue("a" != one);

  assertFalse(one != {valueOf() { return true }});
  assertFalse({valueOf() { return true }} != one);
  assertTrue(two != {valueOf() { return true }});
  assertTrue({valueOf() { return true }} != two);

  assertTrue(Symbol() != zero);
  assertTrue(zero != Symbol());
}

// SameValue
{
  assertTrue(Object.is(zero, zero));
  assertTrue(Object.is(zero, another_zero));
  assertTrue(Object.is(one, one));
  assertTrue(Object.is(one, another_one));
  assertFalse(Object.is(zero, +0));
  assertFalse(Object.is(zero, -0));
  assertFalse(Object.is(+0, zero));
  assertFalse(Object.is(-0, zero));
  assertFalse(Object.is(zero, one));
  assertFalse(Object.is(one, minus_one));
}{
  const obj = Object.defineProperty({}, 'foo',
      {value: zero, writable: false, configurable: false});

  assertTrue(Reflect.defineProperty(obj, 'foo', {value: zero}));
  assertTrue(Reflect.defineProperty(obj, 'foo', {value: another_zero}));
  assertFalse(Reflect.defineProperty(obj, 'foo', {value: one}));
}

// SameValueZero
{
  assertTrue([zero].includes(zero));
  assertTrue([zero].includes(another_zero));

  assertFalse([zero].includes(+0));
  assertFalse([zero].includes(-0));

  assertFalse([+0].includes(zero));
  assertFalse([-0].includes(zero));

  assertTrue([one].includes(one));
  assertTrue([one].includes(another_one));

  assertFalse([one].includes(1));
  assertFalse([1].includes(one));
}{
  assertTrue(new Set([zero]).has(zero));
  assertTrue(new Set([zero]).has(another_zero));

  assertFalse(new Set([zero]).has(+0));
  assertFalse(new Set([zero]).has(-0));

  assertFalse(new Set([+0]).has(zero));
  assertFalse(new Set([-0]).has(zero));

  assertTrue(new Set([one]).has(one));
  assertTrue(new Set([one]).has(another_one));
}{
  assertTrue(new Map([[zero, 42]]).has(zero));
  assertTrue(new Map([[zero, 42]]).has(another_zero));

  assertFalse(new Map([[zero, 42]]).has(+0));
  assertFalse(new Map([[zero, 42]]).has(-0));

  assertFalse(new Map([[+0, 42]]).has(zero));
  assertFalse(new Map([[-0, 42]]).has(zero));

  assertTrue(new Map([[one, 42]]).has(one));
  assertTrue(new Map([[one, 42]]).has(another_one));
}

// Abstract comparison
{
  let undef = Symbol();

  assertTrue(ArkTools.equal(zero, zero));
  assertTrue(ArkTools.greaterThanOrEqual(zero, zero));

  assertTrue(ArkTools.lessThan(zero, one));
  assertTrue(ArkTools.greaterThan(one, zero));

  assertTrue(ArkTools.lessThan(minus_one, one));
  assertTrue(ArkTools.greaterThan(one, minus_one));

  assertTrue(ArkTools.equal(zero, -0));
  assertTrue(ArkTools.lessThanOrEqual(zero, -0));
  assertTrue(ArkTools.greaterThanOrEqual(zero, -0));
  assertTrue(ArkTools.equal(-0, zero));
  assertTrue(ArkTools.lessThanOrEqual(-0, zero));
  assertTrue(ArkTools.greaterThanOrEqual(-0, zero));

  assertTrue(ArkTools.equal(zero, 0));
  assertTrue(ArkTools.equal(0, zero));

  assertTrue(ArkTools.lessThan(minus_one, 1));
  assertTrue(ArkTools.greaterThan(1, minus_one));

  assertFalse(ArkTools.lessThan(six, NaN));
  assertFalse(ArkTools.greaterThan(six, NaN));
  assertFalse(ArkTools.equal(six, NaN));
  assertFalse(ArkTools.lessThan(NaN, six));
  assertFalse(ArkTools.greaterThan(NaN, six));
  assertFalse(ArkTools.equal(NaN, six));

  assertTrue(ArkTools.lessThan(six, Infinity));
  assertTrue(ArkTools.greaterThan(Infinity, six));

  assertTrue(ArkTools.greaterThan(six, -Infinity));
  assertTrue(ArkTools.lessThan(-Infinity, six));

  assertTrue(ArkTools.greaterThan(six, 5.99999999));
  assertTrue(ArkTools.lessThan(5.99999999, six));

  assertTrue(ArkTools.equal(zero, ""));
  assertTrue(ArkTools.lessThanOrEqual(zero, ""));
  assertTrue(ArkTools.greaterThanOrEqual(zero, ""));
  assertTrue(ArkTools.equal("", zero));
  assertTrue(ArkTools.lessThanOrEqual("", zero));
  assertTrue(ArkTools.greaterThanOrEqual("", zero));

  assertTrue(ArkTools.equal(minus_one, "\t-1 "));
  assertTrue(ArkTools.lessThanOrEqual(minus_one, "\t-1 "));
  assertTrue(ArkTools.greaterThanOrEqual(minus_one, "\t-1 "));
  assertTrue(ArkTools.equal("\t-1 ", minus_one));
  assertTrue(ArkTools.lessThanOrEqual("\t-1 ", minus_one));
  assertTrue(ArkTools.greaterThanOrEqual("\t-1 ", minus_one));

  assertFalse(ArkTools.lessThan(minus_one, "-0x1"));
  assertFalse(ArkTools.greaterThan(minus_one, "-0x1"));
  assertFalse(ArkTools.equal(minus_one, "-0x1"));
  assertFalse(ArkTools.lessThan("-0x1", minus_one));
  assertFalse(ArkTools.greaterThan("-0x1", minus_one));
  assertFalse(ArkTools.equal("-0x1", minus_one));

  const unsafe = "9007199254740993";  // 2**53 + 1
  assertFalse(ArkTools.greaterThan(eval(unsafe + "n"), unsafe));
  assertFalse(ArkTools.lessThan(unsafe, eval(unsafe + "n")));

  assertThrows(() => ArkTools.lessThan(six, Symbol(6)), TypeError);
  assertThrows(() => ArkTools.lessThan(Symbol(6), six), TypeError);

  var value_five_string_six = {
      valueOf() { return Object(5); },
      toString() { return 6; }
  };
  assertTrue(ArkTools.lessThanOrEqual(six, value_five_string_six));
  assertTrue(ArkTools.greaterThanOrEqual(six, value_five_string_six));
  assertTrue(ArkTools.lessThanOrEqual(value_five_string_six, six));
  assertTrue(ArkTools.greaterThanOrEqual(value_five_string_six, six));
}{
  assertFalse(zero < zero);
  assertTrue(zero <= zero);

  assertTrue(zero < one);
  assertTrue(zero <= one);
  assertFalse(one < zero);
  assertFalse(one <= zero);

  assertTrue(minus_one < one);
  assertTrue(minus_one <= one);
  assertFalse(one < minus_one);
  assertFalse(one <= minus_one);

  assertFalse(zero < -0);
  assertTrue(zero <= -0);
  assertFalse(-0 < zero);
  assertTrue(-0 <= zero);

  assertFalse(zero < 0);
  assertTrue(zero <= 0);
  assertFalse(0 < zero);
  assertTrue(0 <= zero);

  assertTrue(minus_one < 1);
  assertTrue(minus_one <= 1);
  assertFalse(1 < minus_one);
  assertFalse(1 <= minus_one);

  assertFalse(six < NaN);
  assertFalse(six <= NaN);
  assertFalse(NaN < six);
  assertFalse(NaN <= six);

  assertTrue(six < Infinity);
  assertTrue(six <= Infinity);
  assertFalse(Infinity < six);
  assertFalse(Infinity <= six);

  assertFalse(six < -Infinity);
  assertFalse(six <= -Infinity);
  assertTrue(-Infinity < six);
  assertTrue(-Infinity <= six);

  assertFalse(six < 5.99999999);
  assertFalse(six <= 5.99999999);
  assertTrue(5.99999999 < six);
  assertTrue(5.99999999 <= six);

  assertFalse(zero < "");
  assertTrue(zero <= "");
  assertFalse("" < zero);
  assertTrue("" <= zero);

  assertFalse(minus_one < "\t-1 ");
  assertTrue(minus_one <= "\t-1 ");
  assertFalse("\t-1 " < minus_one);
  assertTrue("\t-1 " <= minus_one);

  assertFalse(minus_one < "-0x1");
  assertFalse(minus_one <= "-0x1");
  assertFalse("-0x1" < minus_one);
  assertFalse("-0x1" <= minus_one);

  const unsafe = "9007199254740993";  // 2**53 + 1
  assertFalse(eval(unsafe + "n") < unsafe);
  assertTrue(eval(unsafe + "n") <= unsafe);
  assertFalse(unsafe < eval(unsafe + "n"));
  assertTrue(unsafe <= eval(unsafe + "n"));

  assertThrows(() => six < Symbol(6), TypeError);
  assertThrows(() => six <= Symbol(6), TypeError);
  assertThrows(() => Symbol(6) < six, TypeError);
  assertThrows(() => Symbol(6) <= six, TypeError);

  assertFalse(six < {valueOf() {return Object(5)}, toString() {return 6}});
  assertTrue(six <= {valueOf() {return Object(5)}, toString() {return 6}});
  assertFalse({valueOf() {return Object(5)}, toString() {return 6}} < six);
  assertTrue({valueOf() {return Object(5)}, toString() {return 6}} <= six);
}
