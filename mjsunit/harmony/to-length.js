// Copyright 2015 the V8 project authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Flags: --allow-natives-syntax

assertEquals(0, ArkTools.toLength(NaN));

assertEquals(0, ArkTools.toLength(-Infinity));

assertEquals(0, ArkTools.toLength(0));

assertEquals(0, ArkTools.toLength(.5));

assertEquals(42, ArkTools.toLength(42.99999));

assertEquals(9007199254740991, ArkTools.toLength(9007199254740991));

assertEquals(9007199254740991, ArkTools.toLength(Infinity));

assertEquals(0, ArkTools.toLength(null));

assertEquals(1, ArkTools.toLength(true));

assertEquals(0, ArkTools.toLength(false));

assertEquals(0, ArkTools.toLength(undefined));

assertEquals(0, ArkTools.toLength("-1"));
assertEquals(123, ArkTools.toLength("123"));
assertEquals(0, ArkTools.toLength("random text"));

assertThrows(function() { ArkTools.toLength(Symbol.toPrimitive) }, TypeError);

var a = { toString: function() { return 54321 }};
assertEquals(54321, ArkTools.toLength(a));

var b = { valueOf: function() { return 42 }};
assertEquals(42, ArkTools.toLength(b));

var c = {
  toString: function() { return "x"},
  valueOf: function() { return 123 }
};
assertEquals(123, ArkTools.toLength(c));

var d = {
  [Symbol.toPrimitive]: function(hint) {
    assertEquals("number", hint);
    return 987654321;
  }
};
assertEquals(987654321, ArkTools.toLength(d));

var e = new Date(0);
assertEquals(0, ArkTools.toLength(e));
