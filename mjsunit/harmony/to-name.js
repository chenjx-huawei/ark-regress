// Copyright 2015 the V8 project authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Flags: --allow-natives-syntax

assertEquals("1", ArkTools.toName(1));

assertEquals("0.5", ArkTools.toName(.5));

assertEquals("null", ArkTools.toName(null));

assertEquals("true", ArkTools.toName(true));

assertEquals("false", ArkTools.toName(false));

assertEquals("undefined", ArkTools.toName(undefined));

assertEquals("random text", ArkTools.toName("random text"));

assertEquals(Symbol.toPrimitive, ArkTools.toName(Symbol.toPrimitive));

var a = { toString: function() { return "xyz" }};
assertEquals("xyz", ArkTools.toName(a));

var b = { valueOf: function() { return 42 }};
assertEquals("[object Object]", ArkTools.toName(b));

var c = {
  toString: function() { return "x"},
  valueOf: function() { return 123 }
};
assertEquals("x", ArkTools.toName(c));

var d = {
  [Symbol.toPrimitive]: function(hint) { return hint }
};
assertEquals("string", ArkTools.toName(d));

var e = new Date(0);
assertEquals(e.toString(), ArkTools.toName(e));
