// Copyright 2015 the V8 project authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Flags: --allow-natives-syntax

assertEquals(1, ArkTools.toNumber(1));

assertEquals(.5, ArkTools.toNumber(.5));

assertEquals(0, ArkTools.toNumber(null));

assertEquals(1, ArkTools.toNumber(true));

assertEquals(0, ArkTools.toNumber(false));

assertEquals(NaN, ArkTools.toNumber(undefined));

assertEquals(-1, ArkTools.toNumber("-1"));
assertEquals(123, ArkTools.toNumber("123"));
assertEquals(NaN, ArkTools.toNumber("random text"));

assertThrows(function() { ArkTools.toNumber(Symbol.toPrimitive) }, TypeError);

var a = { toString: function() { return 54321 }};
assertEquals(54321, ArkTools.toNumber(a));

var b = { valueOf: function() { return 42 }};
assertEquals(42, ArkTools.toNumber(b));

var c = {
  toString: function() { return "x"},
  valueOf: function() { return 123 }
};
assertEquals(123, ArkTools.toNumber(c));

var d = {
  [Symbol.toPrimitive]: function(hint) {
    assertEquals("number", hint);
    return 987654321;
  }
};
assertEquals(987654321, ArkTools.toNumber(d));

var e = new Date(0);
assertEquals(0, ArkTools.toNumber(e));
