// Copyright 2015 the V8 project authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Flags: --allow-natives-syntax

assertEquals("1", ArkTools.toString(1));

assertEquals("0.5", ArkTools.toString(.5));

assertEquals("null", ArkTools.toString(null));

assertEquals("true", ArkTools.toString(true));

assertEquals("false", ArkTools.toString(false));

assertEquals("undefined", ArkTools.toString(undefined));

assertEquals("random text", ArkTools.toString("random text"));

assertThrows(function() { ArkTools.toString(Symbol.toPrimitive) }, TypeError);

var a = { toString: function() { return "xyz" }};
assertEquals("xyz", ArkTools.toString(a));

var b = { valueOf: function() { return 42 }};
assertEquals("[object Object]", ArkTools.toString(b));

var c = {
  toString: function() { return "x"},
  valueOf: function() { return 123 }
};
assertEquals("x", ArkTools.toString(c));

var d = {
  [Symbol.toPrimitive]: function(hint) { return hint }
};
assertEquals("string", ArkTools.toString(d));

var e = new Date(0);
assertEquals(e.toString(), ArkTools.toString(e));
