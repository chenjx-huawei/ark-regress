// Copyright 2017 the V8 project authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Flags: --allow-natives-syntax
// Resources: test/mjsunit/harmony/modules-skip-1.mjs

let ran = false;
async function test1() {
  try {
    let x = { toString() { return 'modules-skip-1.mjs' } };
    let namespace = await import(x);
    let life = namespace.life();
    assertEquals(42, life);
    ran = true;
  } catch(e) {
    ArkTools.abortJS('failure: '+ e);
  }
}

test1();
ArkTools.performMicrotaskCheckpoint();
assertTrue(ran);

ran = false;
async function test2() {
  try {
    let x = { get toString() { return () => 'modules-skip-1.mjs' } };
    let namespace = await import(x);
    let life = namespace.life();
    assertEquals(42, life);
    ran = true;
  } catch(e) {
    ArkTools.abortJS('failure: '+ e);
  }
}

test2();
ArkTools.performMicrotaskCheckpoint();
assertTrue(ran);
