// Copyright 2020 the V8 project authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Flags: --allow-natives-syntax --fuzzing

// Test allow/block-listed intrinsics in the context of fuzzing.

// Blocklisted intrinsics are replaced with undefined.
assertEquals(undefined, ArkTools.constructConsString("a", "b"));

// Blocklisted intrinsics can have wrong arguments.
assertEquals(undefined, ArkTools.constructConsString(1, 2, 3, 4));

// We don't care if an intrinsic actually exists.
assertEquals(undefined, ArkTools.fooBar());

// Check allowlisted intrinsic.
assertNotEquals(undefined, ArkTools.isBeingInterpreted());

// Allowlisted runtime functions with too few args are ignored.
assertEquals(undefined, ArkTools.deoptimizeFunction());

// Superfluous arguments are ignored.
ArkTools.deoptimizeFunction(function() {}, undefined);
assertNotEquals(undefined, ArkTools.isBeingInterpreted(1, 2, 3));
