// Copyright 2023 the V8 project authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.
//
// Flags: --allow-natives-syntax --maglev

const floats = new Float64Array(10);
let v0;
let rest;
function f(proto) {
  const o = {
      __proto__: proto,
  };
  o.h = 1601;
  o.name;
  [v0, ...rest] = floats;
  return o;
}

ArkTools.prepareFunctionForOptimization(f);

for (let i = 0; i < 20; ++i) {
  f();
  const o = f({});
  o.h = v0;
}

ArkTools.optimizeMaglevOnNextCall(f);

f();
