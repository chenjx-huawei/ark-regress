// Copyright 2022 the V8 project authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Flags: --allow-natives-syntax --maglev --no-always-turbofan

// Test LdaModuleVariable with imports.
import {b} from './lda-module-variable-import.mjs'

function bar(y) {
  // b = 1.
  return b + y;
};
ArkTools.prepareFunctionForOptimization(bar);
assertEquals(2, bar(1));
assertEquals(3, bar(2));
ArkTools.optimizeMaglevOnNextCall(bar);
assertEquals(2, bar(1));
assertEquals(3, bar(2));
assertTrue(isMaglevved(bar));

// Test LdaModuleVariable with exports.
export let x = 1;
function foo(y) { return x + y };

ArkTools.prepareFunctionForOptimization(foo);
assertEquals(2, foo(1));
assertEquals(3, foo(2));
ArkTools.optimizeMaglevOnNextCall(foo);
assertEquals(2, foo(1));
assertEquals(3, foo(2));
assertTrue(isMaglevved(foo));

