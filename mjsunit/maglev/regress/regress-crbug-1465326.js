// Copyright 2023 the V8 project authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Flags: --maglev --allow-natives-syntax

class A {}

var x = Function;

class B extends A {
  constructor() {
    x = new.target;
    super();
  }
}
function construct() {
  return Reflect.construct(B, [], Function);
}
ArkTools.prepareFunctionForOptimization(B);
construct();
construct();
ArkTools.optimizeMaglevOnNextCall(B);
var arr = construct();
print(arr.prototype);
