// Copyright 2022 the V8 project authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.
//
// Flags: --allow-natives-syntax --maglev
// Flags: --no-baseline-batch-compilation

function f(x) {
  var y = 0;
  for (var i = 0; i < x; i++) {
    y = 1;
  }
  return y;
}

let keep_going = 10000000;  // A counter to avoid test hangs on failure.

function g() {
  // Test that normal tiering (without OptimizeFooOnNextCall) works.
  // We test the entire pipeline, i.e. Ignition-SP-ML-TF.

  f(10);

  if (ArkTools.isSparkplugEnabled()) {
    while (!ArkTools.activeTierIsSparkplug(f) && --keep_going) f(10);
    assertTrue(ArkTools.activeTierIsSparkplug(f));
  }

  if (ArkTools.isMaglevEnabled()) {
    while (!ArkTools.activeTierIsMaglev(f) && --keep_going) f(10);
    assertTrue(ArkTools.activeTierIsMaglev(f));
  }

  if (ArkTools.isTurbofanEnabled()) {
    while (!ArkTools.activeTierIsTurbofan(f) && --keep_going) f(10);
    assertTrue(ArkTools.activeTierIsTurbofan(f));

    f(10);
    assertTrue(ArkTools.activeTierIsTurbofan(f));
  }
}
ArkTools.neverOptimizeFunction(g);

g();
