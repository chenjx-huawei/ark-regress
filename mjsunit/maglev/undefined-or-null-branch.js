// Copyright 2022 the V8 project authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Flags: --allow-natives-syntax --maglev --no-always-turbofan

function foo(x) {
  return x ?? 10
}

ArkTools.prepareFunctionForOptimization(foo);
assertEquals(10, foo(undefined));
assertEquals(10, foo(null));
assertEquals(1, foo(1));
ArkTools.optimizeMaglevOnNextCall(foo);
assertEquals(10, foo(undefined));
assertEquals(10, foo(null));
assertEquals(1, foo(1));
assertTrue(isMaglevved(foo));
