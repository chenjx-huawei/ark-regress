// Copyright 2023 the V8 project authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.
//
// Flags: --allow-natives-syntax --maglev --maglev-inlining

let result;
// Openharmony only support strict mode.
// printExtra, prettyPrint, prettyPrinted => let printExtra, prettyPrint, prettyPrinted.
let printExtra;
let prettyPrint;
let prettyPrinted;

function dontOpt(x) {
  result = x;
}

(function () {
  prettyPrinted = function prettyPrinted(value) {
    return value;
  }
  let maxExtraPrinting = 100;
  prettyPrint = function (value, extra = false) {
    let str = prettyPrinted(value);
    if (extra && maxExtraPrinting-- <= 0) {
      return;
    }
    dontOpt(str);
  };
  printExtra = function (value) {
    prettyPrint(value, true);
  };
})();

function empty() {}
let g;
var foo = function () {
  printExtra();
  g =  empty();
  printExtra((this instanceof Object));
};

ArkTools.prepareFunctionForOptimization(foo);
ArkTools.prepareFunctionForOptimization(printExtra);
ArkTools.prepareFunctionForOptimization(prettyPrint);
ArkTools.prepareFunctionForOptimization(prettyPrinted);
ArkTools.prepareFunctionForOptimization(empty);
foo();
// Openharmony only support strict mode, this instanceof Object is false.
// true => false
assertEquals(false, result);
ArkTools.optimizeMaglevOnNextCall(foo);
foo();
// Openharmony only support strict mode, this instanceof Object is false.
// true => false
assertEquals(false, result);
