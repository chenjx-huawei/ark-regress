// Copyright 2022 the V8 project authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.
//
// Flags: --shared-string-table --allow-natives-syntax --stress-compaction

function foo() { return "foo"; }

ArkTools.prepareFunctionForOptimization(foo);
let value = foo();
assertTrue(ArkTools.isSharedString(value));
ArkTools.optimizeFunctionOnNextCall(foo);
value = foo();
assertTrue(ArkTools.isSharedString(value));
ArkTools.sharedGC();
value = foo();
assertTrue(ArkTools.isSharedString(value));
assertEquals("foo", value);
