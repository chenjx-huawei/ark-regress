// Copyright 2018 the V8 project authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Flags: --allow-natives-syntax --turbofan --no-lazy-feedback-allocation

function TestSetWithCustomIterator(ctor) {
  const k1 = {};
  const k2 = {};
  let callCount = 0;
  Array.prototype[Symbol.iterator] = () => ({
    next: () =>
      callCount++ === 0
        ? { value: k2, done: false }
        : { done: true }
  });
  const entries = [k1];
  const set = new ctor(entries);
  assertFalse(set.has(k1));
  assertTrue(set.has(k2));
  assertEquals(2, callCount);
}
ArkTools.prepareFunctionForOptimization(TestSetWithCustomIterator);
TestSetWithCustomIterator(Set);
TestSetWithCustomIterator(Set);
TestSetWithCustomIterator(Set);
ArkTools.optimizeFunctionOnNextCall(TestSetWithCustomIterator);
TestSetWithCustomIterator(Set);
assertOptimized(TestSetWithCustomIterator);

TestSetWithCustomIterator(WeakSet);
ArkTools.prepareFunctionForOptimization(TestSetWithCustomIterator);
TestSetWithCustomIterator(WeakSet);
TestSetWithCustomIterator(WeakSet);
ArkTools.optimizeFunctionOnNextCall(TestSetWithCustomIterator);
TestSetWithCustomIterator(WeakSet);
assertOptimized(TestSetWithCustomIterator);

function TestMapWithCustomIterator(ctor) {
  const k1 = {};
  const k2 = {};
  let callCount = 0;
  Array.prototype[Symbol.iterator] = () => ({
    next: () =>
      callCount++ === 0
        ? { value: [k2, 2], done: false }
        : { done: true }
  });
  const entries = [[k1, 1]];
  const map = new ctor(entries);
  assertFalse(map.has(k1));
  assertEquals(2, map.get(k2));
  assertEquals(2, callCount);
}
ArkTools.prepareFunctionForOptimization(TestMapWithCustomIterator);
TestMapWithCustomIterator(Map);
TestMapWithCustomIterator(Map);
TestMapWithCustomIterator(Map);
ArkTools.optimizeFunctionOnNextCall(TestMapWithCustomIterator);
TestMapWithCustomIterator(Map);
assertOptimized(TestMapWithCustomIterator);

TestMapWithCustomIterator(WeakMap);
ArkTools.prepareFunctionForOptimization(TestMapWithCustomIterator);
TestMapWithCustomIterator(WeakMap);
TestMapWithCustomIterator(WeakMap);
ArkTools.optimizeFunctionOnNextCall(TestMapWithCustomIterator);
TestMapWithCustomIterator(WeakMap);
assertOptimized(TestMapWithCustomIterator);
