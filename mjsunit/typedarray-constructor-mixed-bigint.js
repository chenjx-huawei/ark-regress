// Copyright 2021 the V8 project authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

let BigIntCtors = [BigInt64Array, BigUint64Array];
let NonBigIntCtors = [Int8Array,
                      Uint8Array,
                      Uint8ClampedArray,
                      Int16Array,
                      Uint16Array,
                      Int32Array,
                      Uint32Array,
                      Float32Array,
                      Float64Array];
// The typeError message returned by openharmony's 
// TypedArrayHelper::CreateFromTypedArray is 'srcArrayContentType is not equal objContentType'.
// Change from 'Cannot mix BigInt' to 'srcArrayContentType is not equal objContentType'.
function assertThrowsCannotMixBigInt(cb) {
  assertThrows(cb, TypeError, /srcArrayContentType is not equal objContentType/);
}

for (let bigIntTA of BigIntCtors) {
  for (let nonBigIntTA of NonBigIntCtors) {
    assertThrowsCannotMixBigInt(() => { new bigIntTA(new nonBigIntTA(0)); });
    assertThrowsCannotMixBigInt(() => { new bigIntTA(new nonBigIntTA(1)); });

    assertThrowsCannotMixBigInt(() => { new nonBigIntTA(new bigIntTA(0)); });
    assertThrowsCannotMixBigInt(() => { new nonBigIntTA(new bigIntTA(1)); });
  }
}
