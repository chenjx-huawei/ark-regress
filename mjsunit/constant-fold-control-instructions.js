// Copyright 2014 the V8 project authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Flags: --allow-natives-syntax

function test() {
  assertEquals("string", typeof "");
  assertEquals("number", typeof 1.1);
  assertEquals("number", typeof 1);
  assertEquals("boolean", typeof true);
  assertEquals("function", typeof function() {});
  assertEquals("object", typeof null);
  assertEquals("object", typeof {});
  assertEquals("object", typeof /regex/);

  assertTrue(ArkTools.isSmi(1));
  assertFalse(ArkTools.isSmi(1.1));
  assertFalse(ArkTools.isSmi({}));

  assertTrue(ArkTools.isArray([1]));
  assertFalse(ArkTools.isArray(function() {}));

  assertTrue(ArkTools.isJSReceiver(new Date()));
  assertFalse(ArkTools.isJSReceiver(1));
}

ArkTools.prepareFunctionForOptimization(test);
test();
test();
ArkTools.optimizeFunctionOnNextCall(test);
test();
