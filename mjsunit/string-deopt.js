// Copyright 2018 the V8 project authors. All rights reserved.
// Use of this source code is governed by a BSD-style license
// that can be found in the LICENSE file.

// Flags: --allow-natives-syntax --turbofan --no-always-turbofan

(()=> {
  function f(a, b, c) {
    return a.indexOf(b, c);
  };
  ArkTools.prepareFunctionForOptimization(f);
  f("abc", "de", 1);
  f("abc", "de", 1);
  ArkTools.optimizeFunctionOnNextCall(f);
  f("abc", "de", {});
  ArkTools.prepareFunctionForOptimization(f);
  ArkTools.optimizeFunctionOnNextCall(f);
  f("abc", "de", {});
  assertOptimized(f);
})();

(()=> {
  function f(a, b, c) {
    return a.indexOf(b, c);
  };
  ArkTools.prepareFunctionForOptimization(f);
  f("abc", "de", 1);
  f("abc", "de", 1);
  ArkTools.optimizeFunctionOnNextCall(f);
  f("abc", {}, 1);
  ArkTools.prepareFunctionForOptimization(f);
  ArkTools.optimizeFunctionOnNextCall(f);
  f("abc", {}, 1);
  assertOptimized(f);
})();

(()=> {
  function f(a, b, c) {
    return a.substring(b, c);
  };
  ArkTools.prepareFunctionForOptimization(f);
  f("abcde", 1, 4);
  f("abcde", 1, 4);
  ArkTools.optimizeFunctionOnNextCall(f);
  f("abcde", 1, {});
  ArkTools.prepareFunctionForOptimization(f);
  ArkTools.optimizeFunctionOnNextCall(f);
  f("abcde", 1, {});
  assertOptimized(f);
})();

(()=> {
  function f(a, b, c) {
    return a.substring(b, c);
  };
  ArkTools.prepareFunctionForOptimization(f);
  f("abcde", 1, 4);
  f("abcde", 1, 4);
  ArkTools.optimizeFunctionOnNextCall(f);
  f("abcde", {}, 4);
  ArkTools.prepareFunctionForOptimization(f);
  ArkTools.optimizeFunctionOnNextCall(f);
  f("abcde", {}, 4);
  assertOptimized(f);
})();
