// Copyright 2020 the V8 project authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Flags: --allow-natives-syntax --turbofan

import { getImportMeta } from 'modules-skip-import-meta-export.mjs';

function foo() {
  return import.meta;
}
ArkTools.prepareFunctionForOptimization(foo);
ArkTools.optimizeFunctionOnNextCall(foo);
// Optimize when import.meta hasn't been created yet.
assertEquals('object', typeof foo());
assertEquals(import.meta, foo());
assertOptimized(foo);

function bar() {
  return import.meta;
}
ArkTools.prepareFunctionForOptimization(bar);
// Optimize when import.meta already exists.
ArkTools.optimizeFunctionOnNextCall(bar);
assertEquals(import.meta, bar());
assertOptimized(bar);

ArkTools.prepareFunctionForOptimization(getImportMeta);
ArkTools.optimizeFunctionOnNextCall(getImportMeta);
assertEquals('object', typeof getImportMeta());
assertOptimized(getImportMeta);
assertNotEquals(import.meta, getImportMeta());
assertOptimized(getImportMeta);


function baz() {
  return getImportMeta();
}

// Test inlined (from another module) import.meta accesses.
ArkTools.prepareFunctionForOptimization(baz);
baz();
ArkTools.optimizeFunctionOnNextCall(baz);
assertEquals('object', typeof baz());
assertNotEquals(import.meta, baz());
assertEquals(baz(), getImportMeta());
assertOptimized(baz);
