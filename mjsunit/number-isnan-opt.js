// Copyright 2017 the V8 project authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Flags: --allow-natives-syntax

(function() {
  function foo(x) { return Number.isNaN(x); }

  ArkTools.prepareFunctionForOptimization(foo);
  assertTrue(foo(+undefined));
  assertFalse(foo(undefined));
  ArkTools.optimizeFunctionOnNextCall(foo);
  assertTrue(foo(+undefined));
  assertFalse(foo(undefined));
})();

(function() {
  function foo(x) { return Number.isNaN(+x); }

  ArkTools.prepareFunctionForOptimization(foo);
  assertTrue(foo(+undefined));
  assertFalse(foo(0));
  ArkTools.optimizeFunctionOnNextCall(foo);
  assertTrue(foo(+undefined));
  assertFalse(foo(0));
})();

(function() {
  function foo(x) { return Number.isNaN(x|0); }

  ArkTools.prepareFunctionForOptimization(foo);
  assertFalse(foo(+undefined));
  assertFalse(foo(0));
  ArkTools.optimizeFunctionOnNextCall(foo);
  assertFalse(foo(+undefined));
  assertFalse(foo(0));
})();

(function() {
  function foo(x) { return Number.isNaN("" + x); }

  ArkTools.prepareFunctionForOptimization(foo);
  assertFalse(foo(undefined));
  assertFalse(foo(0));
  ArkTools.optimizeFunctionOnNextCall(foo);
  assertFalse(foo(undefined));
  assertFalse(foo(0));
})();

(function() {
  function foo(x) { return Number.isNaN(0/0); }

  ArkTools.prepareFunctionForOptimization(foo);
  assertTrue(foo());
  assertTrue(foo());
  ArkTools.optimizeFunctionOnNextCall(foo);
  assertTrue(foo());
  assertTrue(foo());
})();
