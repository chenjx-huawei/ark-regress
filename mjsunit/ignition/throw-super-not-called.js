// Copyright 2016 the V8 project authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Flags: --turbofan --allow-natives-syntax --no-always-turbofan

class A {
  constructor() { }
}
class B extends A {
  constructor(call_super) {
    if (call_super) {
      super();
    }
  }
}

// Openharmony only support strict mode. test => var test.
var test;

test = new B(1);
assertThrows(() => {new B(0)}, ReferenceError);

ArkTools.prepareFunctionForOptimization(B);
test = new B(1);
test = new B(1);
ArkTools.optimizeFunctionOnNextCall(B);
test = new B(1);
assertOptimized(B);
// Check that hole checks are handled correctly in optimized code.
assertThrows(() => {new B(0)}, ReferenceError);
assertOptimized(B);
