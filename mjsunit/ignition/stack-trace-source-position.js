// Copyright 2016 the V8 project authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Flags: --no-turbo

// TODO(yangguo): fix for turbofan

function f(x) {
  if (x == 0) {
    return new Error().stack;
  }
  return f(x - 1);
}

var stack_lines = f(2).split("\n");

// Openharmony throw stack is different from v8. `11:12` => `13:13`
assertTrue(/at f \(.*?:13:13\)/.test(stack_lines[1]));
// Openharmony throw stack is different from v8. `13:10` => `13:13`
assertTrue(/at f \(.*?:13:13\)/.test(stack_lines[2]));
// Openharmony throw stack is different from v8. 
// `at f` => `at func_main_0`
// `13:10` => `16:16`
assertTrue(/at func_main_0 \(.*?:16:16\)/.test(stack_lines[3]));
