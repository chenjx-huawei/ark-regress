// Copyright 2018 the V8 project authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Flags: --allow-natives-syntax --turbofan --no-always-turbofan

(()=> {
  function f(a, b, c) {
    return String.prototype.indexOf.call(a, b, c);
  }
  ArkTools.prepareFunctionForOptimization(f);
  f("abc", "de", 1);
  f("abc", "de", 1);
  ArkTools.optimizeFunctionOnNextCall(f);
  f("abc", "de", {});
  ArkTools.prepareFunctionForOptimization(f);
  ArkTools.optimizeFunctionOnNextCall(f);
  f("abc", "de", {});
  assertOptimized(f);
})();

(()=> {
  function f(a, b, c) {
    return String.prototype.indexOf.apply(a, [b, c]);
  }
  ArkTools.prepareFunctionForOptimization(f);
  f("abc", "de", 1);
  f("abc", "de", 1);
  ArkTools.optimizeFunctionOnNextCall(f);
  f("abc", {}, 1);
  ArkTools.prepareFunctionForOptimization(f);
  ArkTools.optimizeFunctionOnNextCall(f);
  f("abc", {}, 1);
  assertOptimized(f);
})();

(()=> {
  function f(a, b, c) {
    return Reflect.apply(String.prototype.indexOf, a, [b, c]);
  }
  ArkTools.prepareFunctionForOptimization(f);
  f("abc", "de", 1);
  f("abc", "de", 1);
  ArkTools.prepareFunctionForOptimization(f);
  ArkTools.optimizeFunctionOnNextCall(f);
  f({}, "de", 1);
  ArkTools.prepareFunctionForOptimization(f);
  ArkTools.optimizeFunctionOnNextCall(f);
  f({}, "de", 1);
  assertOptimized(f);
})();

(()=> {
  function f(a, b) {
    return String.fromCharCode.call(a, b);
  }
  ArkTools.prepareFunctionForOptimization(f);
  f("abc", 1);
  f("abc", 1);
  ArkTools.optimizeFunctionOnNextCall(f);
  f("abc", {});
  ArkTools.prepareFunctionForOptimization(f);
  ArkTools.optimizeFunctionOnNextCall(f);
  f({}, {});
  assertOptimized(f);
})();

(()=> {
  function f(a, b) {
    return String.fromCharCode.apply(undefined, [b, {}]);
  }
  ArkTools.prepareFunctionForOptimization(f);
  f("abc", 1);
  f("abc", 1);
  ArkTools.optimizeFunctionOnNextCall(f);
  f("abc", {});
  ArkTools.prepareFunctionForOptimization(f);
  ArkTools.optimizeFunctionOnNextCall(f);
  f("abc", {});
  assertOptimized(f);
})();


(()=> {
  function f(a, b) {
    return Reflect.apply(String.fromCharCode, a, [b, {}]);
  }
  ArkTools.prepareFunctionForOptimization(f);
  f("abc", 1);
  f("abc", 1);
  ArkTools.optimizeFunctionOnNextCall(f);
  f("abc", {});
  ArkTools.prepareFunctionForOptimization(f);
  ArkTools.optimizeFunctionOnNextCall(f);
  f("abc", {});
  assertOptimized(f);
})();
