// Copyright 2023 the V8 project authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Flags: --expose-wasm --allow-natives-syntax --wasm-lazy-compilation

d8.file.execute("test/mjsunit/wasm/wasm-module-builder.js");

const builder = new WasmModuleBuilder();
builder.addFunction('f1', kSig_i_i).addBody([kExprLocalGet, 0]).exportFunc();
builder.addFunction('f2', kSig_i_i).addBody([kExprLocalGet, 0]).exportFunc();
builder.addFunction('f3', kSig_i_i).addBody([kExprLocalGet, 0]).exportFunc();

const instance = builder.instantiate();

instance.exports.f1(1);
instance.exports.f2(2);
instance.exports.f3(3);

assertTrue(ArkTools.isLiftoffFunction(instance.exports.f1));
assertTrue(ArkTools.isLiftoffFunction(instance.exports.f2));
assertTrue(ArkTools.isLiftoffFunction(instance.exports.f3));

ArkTools.flushWasmCode();

assertTrue(ArkTools.isUncompiledWasmFunction(instance.exports.f1));
assertTrue(ArkTools.isUncompiledWasmFunction(instance.exports.f2));
assertTrue(ArkTools.isUncompiledWasmFunction(instance.exports.f3));

instance.exports.f1(1);
instance.exports.f2(2);
instance.exports.f3(3);

ArkTools.wasmTierUpFunction(instance.exports.f3);

ArkTools.flushWasmCode();

assertTrue(ArkTools.isUncompiledWasmFunction(instance.exports.f1));
assertTrue(ArkTools.isUncompiledWasmFunction(instance.exports.f2));
assertTrue(ArkTools.isTurboFanFunction(instance.exports.f3));
