// Copyright 2023 the V8 project authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Flags: --allow-natives-syntax

d8.file.execute('test/mjsunit/wasm/wasm-module-builder.js');

(function enterAndLeaveDebugging() {
  const builder = new WasmModuleBuilder();
  builder.addFunction("main", kSig_i_v).exportFunc().addBody(wasmI32Const(42));
  let main = builder.instantiate().exports.main;
  assertEquals(42, main());
  assertTrue(ArkTools.isLiftoffFunction(main));
  ArkTools.wasmTierUpFunction(main);
  assertEquals(42, main());
  assertTrue(ArkTools.isTurboFanFunction(main));
  ArkTools.wasmEnterDebugging();
  assertEquals(42, main());
  assertTrue(ArkTools.isWasmDebugFunction(main));
  ArkTools.wasmLeaveDebugging();
  ArkTools.wasmTierUpFunction(main);
  assertEquals(42, main());
  assertTrue(ArkTools.isTurboFanFunction(main));
})();
