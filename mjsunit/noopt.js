// Copyright 2017 the V8 project authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Flags: --allow-natives-syntax --noturbofan --no-always-turbofan


// Check that --noturbofan actually works.

function f() {
  return 42;
};
ArkTools.prepareFunctionForOptimization(f);
f();
f();
ArkTools.optimizeFunctionOnNextCall(f);
f();

assertUnoptimized(f);
