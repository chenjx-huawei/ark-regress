// Copyright 2016 the V8 project authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Flags: --max-semi-space-size=1 --allow-natives-syntax

var test_id = 0;

function testCeil(expect, input) {
  // Openharmony only support strict mode. not support Function(). Function() => function() {}
  function test(n) {
    test_id++;
    return Math.ceil(n);
  }
  ArkTools.prepareFunctionForOptimization(test);
  assertEquals(expect, test(input));
  assertEquals(expect, test(input));
  assertEquals(expect, test(input));
  ArkTools.optimizeFunctionOnNextCall(test);
  assertEquals(expect, test(input));

  // Openharmony only support strict mode. not support Function(). Function() => function() {}
  function test_double_input(n) {
    test_id++;
    return Math.ceil(+n);
  }
  ArkTools.prepareFunctionForOptimization(test_double_input);
  assertEquals(expect, test_double_input(input));
  assertEquals(expect, test_double_input(input));
  assertEquals(expect, test_double_input(input));
  ArkTools.optimizeFunctionOnNextCall(test_double_input);
  assertEquals(expect, test_double_input(input));

  // Openharmony only support strict mode. not support Function(). Function() => function() {}
  function test_double_output(n) {
    test_id++;
    return Math.ceil(n) + -0.0;
  }
  ArkTools.prepareFunctionForOptimization(test_double_output);
  assertEquals(expect, test_double_output(input));
  assertEquals(expect, test_double_output(input));
  assertEquals(expect, test_double_output(input));
  ArkTools.optimizeFunctionOnNextCall(test_double_output);
  assertEquals(expect, test_double_output(input));

  // Openharmony only support strict mode. not support Function(). Function() => function() {}
  function test_via_floor(n) {
    test_id++;
    return -Math.floor(-n);
  }
  ArkTools.prepareFunctionForOptimization(test_via_floor);
  assertEquals(expect, test_via_floor(input));
  assertEquals(expect, test_via_floor(input));
  assertEquals(expect, test_via_floor(input));
  ArkTools.optimizeFunctionOnNextCall(test_via_floor);
  assertEquals(expect, test_via_floor(input));

  if (input <= 0) {
    // Openharmony only support strict mode. not support Function(). Function() => function() {}
    function test_via_trunc(n) {
      test_id++;
      return Math.trunc(n);
    }
    ArkTools.prepareFunctionForOptimization(test_via_trunc);
    assertEquals(expect, test_via_trunc(input));
    assertEquals(expect, test_via_trunc(input));
    assertEquals(expect, test_via_trunc(input));
    ArkTools.optimizeFunctionOnNextCall(test_via_trunc);
    assertEquals(expect, test_via_trunc(input));
  }
}

function test() {
  testCeil(0, 0);
  testCeil(+0, +0);
  testCeil(-0, -0);
  testCeil(1, 0.49999);
  testCeil(1, 0.6);
  testCeil(1, 0.5);
  testCeil(-0, -0.1);
  testCeil(-0, -0.5);
  testCeil(-0, -0.6);
  testCeil(-1, -1.6);
  testCeil(-0, -0.50001);
  testCeil(Infinity, Infinity);
  testCeil(-Infinity, -Infinity);
}


// Test in a loop to cover the custom IC and GC-related issues.
for (var i = 0; i < 10; i++) {
  test();
  new Array(i * 10000);
}
