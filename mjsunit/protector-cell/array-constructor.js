// Copyright 2020 the V8 project authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Flags: --allow-natives-syntax

assertTrue(ArkTools.arraySpeciesProtector());
assertTrue(ArkTools.promiseSpeciesProtector());
assertTrue(ArkTools.RegExpSpeciesProtector());
assertTrue(ArkTools.typedArraySpeciesProtector());
Object.defineProperty(Array.prototype, "constructor", { value: {} });
assertFalse(ArkTools.arraySpeciesProtector());
assertTrue(ArkTools.promiseSpeciesProtector());
assertTrue(ArkTools.RegExpSpeciesProtector());
assertTrue(ArkTools.typedArraySpeciesProtector());
