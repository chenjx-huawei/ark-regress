// Copyright 2020 the V8 project authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Flags: --allow-natives-syntax

assertTrue(ArkTools.setIteratorProtector());
assertTrue(ArkTools.mapIteratorProtector());
assertTrue(ArkTools.stringIteratorProtector());
assertTrue(ArkTools.arrayIteratorProtector());
const mapIteratorPrototype = Object.getPrototypeOf(new Map().values());
Object.defineProperty(mapIteratorPrototype, "next", { value: {} });
assertTrue(ArkTools.setIteratorProtector());
assertFalse(ArkTools.mapIteratorProtector());
assertTrue(ArkTools.stringIteratorProtector());
assertTrue(ArkTools.arrayIteratorProtector());
