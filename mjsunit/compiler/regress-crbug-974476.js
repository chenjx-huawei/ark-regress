// Copyright 2019 the V8 project authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Flags: --allow-natives-syntax

function use(x) { return x; }
ArkTools.neverOptimizeFunction(use);

function foo() {
  let result = undefined;
  (function () {
    const a = {};
    for (let _ of [0]) {
      const empty = [];
      (function () {
        result = 42;
        for (let _ of [0]) {
          for (let _ of [0]) {
            use(empty);
          }
        }
        result = a;
      })();
    }
  })();
  return result;
}

ArkTools.prepareFunctionForOptimization(foo);
foo();
foo();
ArkTools.optimizeFunctionOnNextCall(foo);
foo();
