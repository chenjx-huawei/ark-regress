// Copyright 2019 the V8 project authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Flags: --allow-natives-syntax

var a = new Int8Array(1);

function f(i) {
  return i in a;
};
ArkTools.prepareFunctionForOptimization(f);
assertTrue(f(0));
ArkTools.optimizeFunctionOnNextCall(f);
assertFalse(f(-1));
