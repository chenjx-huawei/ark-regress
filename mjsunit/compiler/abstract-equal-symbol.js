// Copyright 2018 the V8 project authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Flags: --allow-natives-syntax --turbofan --noalways-turbofan

// Known symbols abstract equality.
(function() {
  const a = Symbol("a");
  const b = Symbol("b");

  function foo() { return a == b; }

  ArkTools.prepareFunctionForOptimization(foo);
  assertFalse(foo());
  assertFalse(foo());
  ArkTools.optimizeFunctionOnNextCall(foo);
  assertFalse(foo());
})();

// Known symbols abstract in-equality.
(function() {
  const a = Symbol("a");
  const b = Symbol("b");

  function foo() { return a != b; }

  ArkTools.prepareFunctionForOptimization(foo);
  assertTrue(foo());
  assertTrue(foo());
  ArkTools.optimizeFunctionOnNextCall(foo);
  assertTrue(foo());
})();

// Known symbol on one side abstract equality.
(function() {
  const a = Symbol("a");
  const b = Symbol("b");

  function foo(a) { return a == b; }

  // Warmup
  ArkTools.prepareFunctionForOptimization(foo);
  assertTrue(foo(b));
  assertFalse(foo(a));
  assertTrue(foo(b));
  assertFalse(foo(a));
  ArkTools.optimizeFunctionOnNextCall(foo);
  assertTrue(foo(b));
  // Re-prepare the function immediately to make sure type feedback isn't
  // cleared by untimely gc, as re-optimization on new feedback is tested below
  ArkTools.prepareFunctionForOptimization(foo);
  assertFalse(foo(a));
  assertOptimized(foo);

  // Make optimized code bail out
  assertFalse(foo("a"));
  assertUnoptimized(foo);

  // Make sure TurboFan learns the new feedback
  ArkTools.optimizeFunctionOnNextCall(foo);
  assertFalse(foo("a"));
  assertOptimized(foo);
})();

// Known symbol on one side abstract in-equality.
(function() {
  const a = Symbol("a");
  const b = Symbol("b");

  function foo(a) { return a != b; }

  // Warmup
  ArkTools.prepareFunctionForOptimization(foo);
  assertFalse(foo(b));
  assertTrue(foo(a));
  assertFalse(foo(b));
  assertTrue(foo(a));
  ArkTools.optimizeFunctionOnNextCall(foo);
  assertFalse(foo(b));
  assertTrue(foo(a));

  // Make optimized code bail out
  assertTrue(foo("a"));
  assertUnoptimized(foo);

  // Make sure TurboFan learns the new feedback
  ArkTools.prepareFunctionForOptimization(foo);
  ArkTools.optimizeFunctionOnNextCall(foo);
  assertTrue(foo("a"));
  assertOptimized(foo);
})();

// Feedback based symbol abstract equality.
(function() {
  const a = Symbol("a");
  const b = Symbol("b");

  function foo(a, b) { return a == b; }

  // Warmup
  ArkTools.prepareFunctionForOptimization(foo);
  assertTrue(foo(b, b));
  assertFalse(foo(a, b));
  assertTrue(foo(a, a));
  assertFalse(foo(b, a));
  ArkTools.optimizeFunctionOnNextCall(foo);
  assertTrue(foo(a, a));
  assertFalse(foo(b, a));

  // Make optimized code bail out
  assertFalse(foo("a", b));
  assertUnoptimized(foo);

  // Make sure TurboFan learns the new feedback
  ArkTools.prepareFunctionForOptimization(foo);
  ArkTools.optimizeFunctionOnNextCall(foo);
  assertFalse(foo("a", b));
  assertOptimized(foo);
})();

// Feedback based symbol abstract in-equality.
(function() {
  const a = Symbol("a");
  const b = Symbol("b");

  function foo(a, b) { return a != b; }

  ArkTools.prepareFunctionForOptimization(foo);
  assertFalse(foo(b, b));
  assertTrue(foo(a, b));
  assertFalse(foo(a, a));
  assertTrue(foo(b, a));
  ArkTools.optimizeFunctionOnNextCall(foo);
  assertFalse(foo(a, a));
  assertTrue(foo(b, a));

  // Make optimized code bail out
  assertTrue(foo("a", b));
  assertUnoptimized(foo);

  // Make sure TurboFan learns the new feedback
  ArkTools.prepareFunctionForOptimization(foo);
  ArkTools.optimizeFunctionOnNextCall(foo);
  assertTrue(foo("a", b));
  assertOptimized(foo);
})();
