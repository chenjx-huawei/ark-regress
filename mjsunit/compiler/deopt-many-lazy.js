// Copyright 2017 the V8 project authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file

// Flags: --allow-natives-syntax


// It optimizes and lazily deoptimizes 2 functions several times.

function f() {
  g();
}

function g() {
  ArkTools.deoptimizeFunction(f);
}

function a() {
  b();
}

function b() {
  ArkTools.deoptimizeFunction(a);
}

ArkTools.prepareFunctionForOptimization(f);
f(); f();
ArkTools.optimizeFunctionOnNextCall(f);
f();
ArkTools.prepareFunctionForOptimization(a);
a(); a();
ArkTools.optimizeFunctionOnNextCall(a);
a();
for(var i = 0; i < 5; i++) {
  ArkTools.prepareFunctionForOptimization(f);
  ArkTools.optimizeFunctionOnNextCall(f);
  f();
  ArkTools.prepareFunctionForOptimization(a);
  ArkTools.optimizeFunctionOnNextCall(a);
  a();
}
