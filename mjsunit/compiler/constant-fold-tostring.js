// Copyright 2017 the V8 project authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Flags: --allow-natives-syntax --no-always-turbofan --turbofan

// Check that constant-folding of ToString operations works properly for NaN.
(function() {
  const foo = () => `${NaN}`;
  ArkTools.prepareFunctionForOptimization(foo);
  assertEquals("NaN", foo());
  assertEquals("NaN", foo());
  ArkTools.optimizeFunctionOnNextCall(foo);
  assertEquals("NaN", foo());
})();

// Check that constant-folding of ToString operations works properly for 0/-0.
(function() {
  const foo = x => `${x ? 0 : -0}`;
  ArkTools.prepareFunctionForOptimization(foo);
  assertEquals("0", foo(true));
  assertEquals("0", foo(false));
  assertEquals("0", foo(true));
  assertEquals("0", foo(false));
  ArkTools.optimizeFunctionOnNextCall(foo);
  assertEquals("0", foo(true));
  assertEquals("0", foo(false));
})();
