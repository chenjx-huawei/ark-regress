// Copyright 2021 the V8 project authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Flags: --allow-natives-syntax

function foo(a) {
  return ((a & 1) == 1) & ((a & 2) == 1);
}

ArkTools.prepareFunctionForOptimization(foo);
assertEquals(0, foo(1));
ArkTools.optimizeFunctionOnNextCall(foo);
assertEquals(0, foo(1));
