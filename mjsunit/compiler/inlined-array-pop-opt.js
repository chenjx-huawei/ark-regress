// Copyright 2016 the V8 project authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Flags: --allow-natives-syntax --turbofan

(function() {
  function foo(a) { return a.pop(); }

  var x = {};
  var a = [x,x,];

  ArkTools.prepareFunctionForOptimization(foo);
  assertEquals(x, foo(a));
  assertEquals(x, foo(a));
  ArkTools.optimizeFunctionOnNextCall(foo);
  assertEquals(undefined, foo(a));
  assertOptimized(foo);
})();

(function() {
  function foo(a) { return a.pop(); }

  var x = 0;
  var a = [x,x,];

  ArkTools.prepareFunctionForOptimization(foo);
  assertEquals(x, foo(a));
  assertEquals(x, foo(a));
  ArkTools.optimizeFunctionOnNextCall(foo);
  assertEquals(undefined, foo(a));
  assertOptimized(foo);
})();

(function() {
  function foo(a) { return a.pop(); }

  var x = 0;
  var a = [x,x,x];

  ArkTools.prepareFunctionForOptimization(foo);
  assertEquals(x, foo(a));
  assertEquals(x, foo(a));
  ArkTools.optimizeFunctionOnNextCall(foo);
  assertEquals(x, foo(a));
  assertOptimized(foo);
})();

(function() {
  function foo(a) { return a.pop(); }

  var x = {};
  var a = [x,x,x];

  ArkTools.prepareFunctionForOptimization(foo);
  assertEquals(x, foo(a));
  assertEquals(x, foo(a));
  ArkTools.optimizeFunctionOnNextCall(foo);
  assertEquals(x, foo(a));
  assertOptimized(foo);
})();

(function() {
  function foo(a) { return a.pop(); }

  var a = [,,];

  ArkTools.prepareFunctionForOptimization(foo);
  assertEquals(undefined, foo(a));
  assertEquals(undefined, foo(a));
  ArkTools.optimizeFunctionOnNextCall(foo);
  assertEquals(undefined, foo(a));
  assertOptimized(foo);
})();

(function() {
  var pop = Array.prototype.pop;

  function foo(a) { return a.pop(); }

  var a = [1, 2, 3];

  ArkTools.prepareFunctionForOptimization(foo);
  assertEquals(3, foo(a));
  assertEquals(2, foo(a));
  ArkTools.optimizeFunctionOnNextCall(foo);
  assertEquals(1, foo(a));
  assertOptimized(foo);
})();
