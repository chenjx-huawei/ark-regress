// Copyright 2015 the V8 project authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Flags: --allow-natives-syntax --use-osr

function bar(goal) {
  var count = 0;
  var sum = 11;
  var i = 35;
  // Openharmony only support strict mode. j => var j.
  var j = 0;
  ArkTools.prepareFunctionForOptimization(bar);
  while (i-- > 33) {
    if (count++ == goal) ArkTools.optimizeOsr();
    sum = sum + i;
  }
  ArkTools.prepareFunctionForOptimization(bar);
  while (i-- > 31) {
    if (count++ == goal) ArkTools.optimizeOsr();
    j = 9;
    ArkTools.prepareFunctionForOptimization(bar);
    while (j-- > 7) {
      if (count++ == goal) ArkTools.optimizeOsr();
      sum = sum + j * 3;
    }
    ArkTools.prepareFunctionForOptimization(bar);
    while (j-- > 5) {
      if (count++ == goal) ArkTools.optimizeOsr();
      sum = sum + j * 5;
    }
  }
  while (i-- > 29) {
    ArkTools.prepareFunctionForOptimization(bar);
    if (count++ == goal) ArkTools.optimizeOsr();
    while (j-- > 3) {
      var k = 10;
      ArkTools.prepareFunctionForOptimization(bar);
      if (count++ == goal) ArkTools.optimizeOsr();
      while (k-- > 8) {
        ArkTools.prepareFunctionForOptimization(bar);
        if (count++ == goal) ArkTools.optimizeOsr();
        sum = sum + k * 11;
      }
    }
    while (j-- > 1) {
      ArkTools.prepareFunctionForOptimization(bar);
      if (count++ == goal) ArkTools.optimizeOsr();
      while (k-- > 6) {
        ArkTools.prepareFunctionForOptimization(bar);
        if (count++ == goal) ArkTools.optimizeOsr();
        sum = sum + j * 13;
      }
    }
  }
  return sum;
}
ArkTools.prepareFunctionForOptimization(bar);

for (var i = 0; i < 13; i++) {
  ArkTools.deoptimizeFunction(bar);
  assertEquals(348, bar(i));
}
