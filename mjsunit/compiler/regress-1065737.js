// Copyright 2020 the V8 project authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Flags: --allow-natives-syntax

function foo() {
  class c {
    static get [v = 0]() {}
  }
}

ArkTools.prepareFunctionForOptimization(foo);
assertThrows(foo, ReferenceError);
assertThrows(foo, ReferenceError);
ArkTools.optimizeFunctionOnNextCall(foo);
assertThrows(foo, ReferenceError);
