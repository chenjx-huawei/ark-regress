// Copyright 2020 the V8 project authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Flags: --allow-natives-syntax --turbofan --no-always-turbofan

// Openharmony only support strict mode. glo => var glo.
var glo = 0;

function write_glo(x) { glo = x }

ArkTools.prepareFunctionForOptimization(write_glo);
write_glo({});
write_glo(1);
assertEquals(1, glo);

// At this point, glo has cell type Mutable.

ArkTools.optimizeFunctionOnNextCall(write_glo);
write_glo(0);
assertEquals(0, glo);
assertOptimized(write_glo);

Object.freeze(this);
assertUnoptimized(write_glo);
write_glo(1);
assertEquals(0, glo);
