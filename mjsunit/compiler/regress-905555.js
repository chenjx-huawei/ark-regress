// Copyright 2018 the V8 project authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Flags: --allow-natives-syntax --noalways-turbofan

this.global = 1;
function boom(value) {
  return global;
}

ArkTools.prepareFunctionForOptimization(boom);
assertEquals(1, boom());
assertEquals(1, boom());
ArkTools.disableOptimizationFinalization();
ArkTools.optimizeFunctionOnNextCall(boom, "concurrent");
assertEquals(1, boom());

ArkTools.waitForBackgroundOptimization();

ArkTools.finalizeOptimization();
Object.defineProperty(this, "global", {
	get() {
	  return 42;
	}
});
// boom should be deoptimized because the global property cell has changed.
assertUnoptimized(boom);

assertEquals(42, boom());
