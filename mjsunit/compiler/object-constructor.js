// Copyright 2017 the V8 project authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Flags: --allow-natives-syntax

// Common pattern in Webpack 3 generated bundles, see
// https://github.com/webpack/webpack/issues/5600 for details.
(function ObjectConstructorWithKnownFunction() {
  "use strict";
  class A {
    bar() { return this; }
  };
  function foo(a) {
    return Object(a.bar)();
  }
  ArkTools.prepareFunctionForOptimization(foo);
  assertEquals(undefined, foo(new A));
  assertEquals(undefined, foo(new A));
  ArkTools.optimizeFunctionOnNextCall(foo);
  assertEquals(undefined, foo(new A));
})();

(function ObjectConstructorWithString() {
  "use strict";
  function foo() {
    return Object("a");
  }
  ArkTools.prepareFunctionForOptimization(foo);
  assertEquals('object', typeof foo());
  assertEquals('object', typeof foo());
  ArkTools.optimizeFunctionOnNextCall(foo);
  assertEquals('object', typeof foo());
})();

// Object constructor subclassing via Class Factories, see
// https://twitter.com/FremyCompany/status/905977048006402048
// for the hint.
(function ObjectConstructorSubClassing() {
  "use strict";
  const Factory = Base => class A extends Base {};
  const A = Factory(Object);
  function foo() {
    return new A(1, 2, 3);
  }
  ArkTools.prepareFunctionForOptimization(foo);
  assertInstanceof(foo(), A);
  assertInstanceof(foo(), Object);
  assertInstanceof(foo(), A);
  assertInstanceof(foo(), Object);
  ArkTools.optimizeFunctionOnNextCall(foo);
  assertInstanceof(foo(), A);
  assertInstanceof(foo(), Object);
})();
