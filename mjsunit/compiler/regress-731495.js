// Copyright 2016 the V8 project authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Flags: --allow-natives-syntax

function foo() {
  let global = "";
  global = global + "bar";
  return global;
};

ArkTools.prepareFunctionForOptimization(foo);
assertEquals(foo(), "bar");
ArkTools.optimizeFunctionOnNextCall(foo);
assertEquals(foo(), "bar");
