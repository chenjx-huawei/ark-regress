// Copyright 2016 the V8 project authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Flags: --allow-natives-syntax

function A() {}
var a = new A();

var B = {
  [Symbol.hasInstance](o) {
    return false;
  }
};
ArkTools.toFastProperties(B.__proto__);

var C = Object.create({
  [Symbol.hasInstance](o) {
    return true;
  }
});
ArkTools.toFastProperties(C.__proto__);

var D = Object.create({
  [Symbol.hasInstance](o) {
    return o === a;
  }
});
ArkTools.toFastProperties(D.__proto__);

var E = Object.create({
  [Symbol.hasInstance](o) {
    if (o === a) throw o;
    return true;
  }
});
ArkTools.toFastProperties(E.__proto__);

function F() {}
F.__proto__ = null;

(function() {
  function foo(o) { return o instanceof A; }

  ArkTools.prepareFunctionForOptimization(foo);
  assertTrue(foo(a));
  assertTrue(foo(a));
  assertTrue(foo(new A()));
  ArkTools.optimizeFunctionOnNextCall(foo);
  assertTrue(foo(a));
  assertTrue(foo(new A()));
})();

(function() {
  function foo(o) {
    try {
      return o instanceof A;
    } catch (e) {
      return e;
    }
  }

  ArkTools.prepareFunctionForOptimization(foo);
  assertTrue(foo(a));
  assertTrue(foo(a));
  assertTrue(foo(new A()));
  assertEquals(1, foo(new Proxy({}, {getPrototypeOf() { throw 1; }})));
  ArkTools.optimizeFunctionOnNextCall(foo);
  assertTrue(foo(a));
  assertTrue(foo(new A()));
  assertEquals(1, foo(new Proxy({}, {getPrototypeOf() { throw 1; }})));
})();

(function() {
  function foo(o) { return o instanceof B; }

  ArkTools.prepareFunctionForOptimization(foo);
  assertFalse(foo(a));
  assertFalse(foo(a));
  assertFalse(foo(new A()));
  ArkTools.optimizeFunctionOnNextCall(foo);
  assertFalse(foo(a));
  assertFalse(foo(new A()));
})();

(function() {
  function foo(o) { return o instanceof C; }

  ArkTools.prepareFunctionForOptimization(foo);
  assertTrue(foo(a));
  assertTrue(foo(a));
  assertTrue(foo(new A()));
  ArkTools.optimizeFunctionOnNextCall(foo);
  assertTrue(foo(a));
  assertTrue(foo(new A()));
})();

(function() {
  function foo(o) { return o instanceof D; }

  ArkTools.prepareFunctionForOptimization(foo);
  assertTrue(foo(a));
  assertTrue(foo(a));
  assertFalse(foo(new A()));
  ArkTools.optimizeFunctionOnNextCall(foo);
  assertTrue(foo(a));
  assertFalse(foo(new A()));
})();

(function() {
  function foo(o) {
    try {
      return o instanceof E;
    } catch (e) {
      return false;
    }
  }

  ArkTools.prepareFunctionForOptimization(foo);
  assertFalse(foo(a));
  assertTrue(foo(new A()));
  ArkTools.optimizeFunctionOnNextCall(foo);
  assertFalse(foo(a));
  assertTrue(foo(new A()));
})();

(function() {
  function foo(o) {
    return o instanceof F;
  }

  ArkTools.prepareFunctionForOptimization(foo);
  assertFalse(foo(a));
  assertFalse(foo(new A()));
  assertTrue(foo(new F()));
  ArkTools.optimizeFunctionOnNextCall(foo);
  assertFalse(foo(a));
  assertFalse(foo(new A()));
  assertTrue(foo(new F()));
})();

(function() {
  function foo() {
    var a = new A();
    return a instanceof A;
  }

  ArkTools.prepareFunctionForOptimization(foo);
  assertTrue(foo());
  assertTrue(foo());
  ArkTools.optimizeFunctionOnNextCall(foo);
  assertTrue(foo());
})();

(function() {
  class B extends A {};

  function makeFoo() {
    return function foo(b) {
      return b instanceof B;
    }
  }
  makeFoo();
  const foo = makeFoo();

  ArkTools.prepareFunctionForOptimization(foo);
  assertTrue(foo(new B));
  assertFalse(foo(new A));
  ArkTools.optimizeFunctionOnNextCall(foo);
  assertTrue(foo(new B));
  assertFalse(foo(new A));
})();
