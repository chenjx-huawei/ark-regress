// Copyright 2016 the V8 project authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Flags: --allow-natives-syntax
"use strict";

function f() {
  try {
    o = 1;
  } catch (e) {
    return e;
  }
  return 0;
}

function deopt() {
  ArkTools.deoptimizeFunction(f);
  throw 42;
}

ArkTools.neverOptimizeFunction(deopt);

Object.defineProperty(this, 'o', {get: deopt});

ArkTools.prepareFunctionForOptimization(f);
f();
f();
ArkTools.optimizeFunctionOnNextCall(f);
assertEquals(42, f());
