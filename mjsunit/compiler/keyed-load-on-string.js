// Copyright 2019 the V8 project authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Flags: --allow-natives-syntax

var s = "hello";

function foo() {
  return s[4];
};
ArkTools.prepareFunctionForOptimization(foo);
assertTrue('o' === foo());
assertTrue("o" === foo());
ArkTools.optimizeFunctionOnNextCall(foo);
assertTrue("o" === foo());

function bar() {
  return s[5];
};
ArkTools.prepareFunctionForOptimization(bar);
assertSame(undefined, bar());
assertSame(undefined, bar());
ArkTools.optimizeFunctionOnNextCall(bar);
assertSame(undefined, bar());
