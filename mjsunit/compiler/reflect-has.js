// Copyright 2017 the V8 project authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Flags: --allow-natives-syntax

// Test Reflect.has with wrong (number of) arguments.
(function() {
  "use strict";
  function foo() { return Reflect.has(); }

  ArkTools.prepareFunctionForOptimization(foo);
  assertThrows(foo);
  assertThrows(foo);
  ArkTools.optimizeFunctionOnNextCall(foo);
  assertThrows(foo);
})();
(function() {
  "use strict";
  function foo(o) { return Reflect.has(o); }

  ArkTools.prepareFunctionForOptimization(foo);
  assertFalse(foo({}));
  assertFalse(foo({}));
  ArkTools.optimizeFunctionOnNextCall(foo);
  assertFalse(foo({}));
})();
(function() {
  "use strict";
  function foo(o) { return Reflect.has(o); }

  ArkTools.prepareFunctionForOptimization(foo);
  assertThrows(foo.bind(undefined, 1));
  assertThrows(foo.bind(undefined, undefined));
  ArkTools.optimizeFunctionOnNextCall(foo);
  assertThrows(foo.bind(undefined, 'o'));
})();

// Test Reflect.has within try/catch.
(function() {
  "use strict";
  function foo() {
    try {
      return Reflect.has();
    } catch (e) {
      return 1;
    }
  }

  ArkTools.prepareFunctionForOptimization(foo);
  assertEquals(1, foo());
  assertEquals(1, foo());
  ArkTools.optimizeFunctionOnNextCall(foo);
  assertEquals(1, foo());
})();
(function() {
  "use strict";
  const o = {};
  function foo(n) {
    try {
      return Reflect.has(o, n);
    } catch (e) {
      return 1;
    }
  }

  ArkTools.prepareFunctionForOptimization(foo);
  assertEquals(1, foo({[Symbol.toPrimitive]() { throw new Error(); }}));
  assertEquals(1, foo({[Symbol.toPrimitive]() { throw new Error(); }}));
  ArkTools.optimizeFunctionOnNextCall(foo);
  assertEquals(1, foo({[Symbol.toPrimitive]() { throw new Error(); }}));
})();
