// Copyright 2015 the V8 project authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Flags: --allow-natives-syntax --use-osr

function id(f) { return f; }

function foo(a) {
  var r = /\0/;
  for (var i = 0; i < 10; i++) {
    if (a) ArkTools.optimizeOsr();
    ArkTools.prepareFunctionForOptimization(foo);
  }
  return r;
}
ArkTools.prepareFunctionForOptimization(foo);

function bar(a) {
  for (var i = 0; i < 10; i++) {
    if (a) ArkTools.optimizeOsr();
    ArkTools.prepareFunctionForOptimization(bar);
    var r = /\0/;
  }
  return r;
}
ArkTools.prepareFunctionForOptimization(bar);

function baz(a) {
  for (var i = 0; i < 10; i++) {
    if (a) ArkTools.optimizeOsr();
    ArkTools.prepareFunctionForOptimization(baz);
  }
  return /\0/;
}
ArkTools.prepareFunctionForOptimization(baz);

function qux(a) {
  for (var i = 0; i < 10; i++) {
    if (i > 5 && a) {
      ArkTools.optimizeOsr();
      ArkTools.prepareFunctionForOptimization(qux);
    } else {
      var r = /\0/;
    }
  }
  return r;
}
ArkTools.prepareFunctionForOptimization(qux);

function test(f) {
  // Test the reference equality of regex's created in OSR'd function.
  var x = f(false);
  ArkTools.prepareFunctionForOptimization(f);
  assertEquals(x, f(true));
  ArkTools.prepareFunctionForOptimization(f);
  assertEquals(x, f(true));
  ArkTools.prepareFunctionForOptimization(f);
  assertEquals(x, f(true));
}

test(foo);
test(bar);
test(baz);
test(qux);
