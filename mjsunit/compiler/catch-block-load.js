// Copyright 2019 the V8 project authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Flags: --allow-natives-syntax --turboshaft-enable-debug-features

const obj = { a: 42 };

function boom() {
  throw "boom";
}

// Ensure that we optimize the monomorphic case.
(function() {
  function bar(x) {
    try {
      boom();
      ++i;
    } catch(_) {
      ArkTools.turbofanStaticAssert(x.a == 42);
      return x.a;
    }
  }

  function foo() {
    return bar(obj);
  }

  ArkTools.prepareFunctionForOptimization(foo);
  ArkTools.prepareFunctionForOptimization(bar);

  foo();
  foo();
  ArkTools.optimizeFunctionOnNextCall(foo);
  foo();
})();

// And the megamorphic case.
(function() {
  function bar(x) {
    try {
      boom();
      ++i;
    } catch(_) {
      ArkTools.turbofanStaticAssert(x.a == 42);
      return x.a;
    }
  }

  function foo() {
    return bar(obj);
  }

  ArkTools.prepareFunctionForOptimization(foo);
  ArkTools.prepareFunctionForOptimization(bar);

  bar({b: 42});
  bar({c: 42});
  bar({d: 42});
  bar({e: 42});
  bar({f: 42});

  foo();
  foo();
  ArkTools.optimizeFunctionOnNextCall(foo);
  foo();
})();
