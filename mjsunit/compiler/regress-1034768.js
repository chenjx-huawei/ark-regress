// Copyright 2019 the V8 project authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Flags: --allow-natives-syntax --max-serializer-nesting=4
function f1() {
  return 1;
}

function f2() { return f1(); }
function f3() { return f2(); }
function f4() { return f3(); }
function f5() { return f4(); }

ArkTools.prepareFunctionForOptimization(f1);
ArkTools.prepareFunctionForOptimization(f2);
ArkTools.prepareFunctionForOptimization(f3);
ArkTools.prepareFunctionForOptimization(f4);
ArkTools.prepareFunctionForOptimization(f5);

f5();
f5();
ArkTools.optimizeFunctionOnNextCall(f5);
f5();
