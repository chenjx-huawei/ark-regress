// Copyright 2016 the V8 project authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Flags: --allow-natives-syntax --turbofan --no-always-turbofan

var s = "12345";

(function() {
  function foo() { return s[5]; }

  ArkTools.prepareFunctionForOptimization(foo);
  foo();
  foo();
  ArkTools.optimizeFunctionOnNextCall(foo);
  foo();
  ArkTools.prepareFunctionForOptimization(foo);
  ArkTools.optimizeFunctionOnNextCall(foo);
  foo();
  assertOptimized(foo);
})();

(function() {
  function foo(i) { return s[i]; }

  ArkTools.prepareFunctionForOptimization(foo);
  foo(0);
  foo(1);
  ArkTools.optimizeFunctionOnNextCall(foo);
  foo(5);
  ArkTools.prepareFunctionForOptimization(foo);
  ArkTools.optimizeFunctionOnNextCall(foo);
  foo(5);
  assertOptimized(foo);
})();

(function() {
  function foo(s) { return s[5]; }

  ArkTools.prepareFunctionForOptimization(foo);
  foo(s);
  foo(s);
  ArkTools.optimizeFunctionOnNextCall(foo);
  foo(s);
  ArkTools.prepareFunctionForOptimization(foo);
  ArkTools.optimizeFunctionOnNextCall(foo);
  foo(s);
  assertOptimized(foo);
})();

(function() {
  function foo(s, i) { return s[i]; }

  ArkTools.prepareFunctionForOptimization(foo);
  foo(s, 0);
  foo(s, 1);
  ArkTools.optimizeFunctionOnNextCall(foo);
  foo(s, 5);
  ArkTools.prepareFunctionForOptimization(foo);
  ArkTools.optimizeFunctionOnNextCall(foo);
  foo(s, 5);
  assertOptimized(foo);
})();
