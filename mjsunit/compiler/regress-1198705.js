// Copyright 2021 the V8 project authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Flags: --allow-natives-syntax

function bar(x) {
  x = (x|0) * 2**52;
  x = Math.min(Math.max(x, 0), 2**52);
  return (x + x)|0;
}

ArkTools.prepareFunctionForOptimization(bar);
assertEquals(0, bar(0));
ArkTools.optimizeFunctionOnNextCall(bar);

function foo() {
  return bar(1);
}

ArkTools.prepareFunctionForOptimization(foo);
assertEquals(0, foo());
ArkTools.optimizeFunctionOnNextCall(foo);
assertEquals(0, foo());
