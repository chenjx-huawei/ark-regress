// Copyright 2016 the V8 project authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Flags: --allow-natives-syntax --turbo-escape

(function TestMaterializeArray() {
  function f() {
    var a = [1,2,3];
    ArkTools._DeoptimizeNow();
    return a.length;
  }
  ArkTools.prepareFunctionForOptimization(f);
  assertEquals(3, f());
  assertEquals(3, f());
  ArkTools.optimizeFunctionOnNextCall(f);
  assertEquals(3, f());
})();

(function TestMaterializeFunction() {
  function g() {
    function fun(a, b) {}
    ArkTools._DeoptimizeNow();
    return fun.length;
  }
  ArkTools.prepareFunctionForOptimization(g);
  assertEquals(2, g());
  assertEquals(2, g());
  ArkTools.optimizeFunctionOnNextCall(g);
  assertEquals(2, g());
})();
