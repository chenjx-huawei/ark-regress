// Copyright 2017 the V8 project authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Flags: --allow-natives-syntax

function foo() { ArkTools.toLength(42n) }
ArkTools.prepareFunctionForOptimization(foo);
assertThrows(foo, TypeError);
ArkTools.optimizeFunctionOnNextCall(foo);
assertThrows(foo, TypeError);
