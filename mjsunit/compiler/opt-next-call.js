// Copyright 2014 the V8 project authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Flags: --allow-natives-syntax --turbofan

function foo() {
  return "fooed";
}

ArkTools.prepareFunctionForOptimization(foo);
ArkTools.optimizeFunctionOnNextCall(foo);
assertEquals("fooed", foo());
assertOptimized(foo);

function bar() {
  return "bared";
}

ArkTools.prepareFunctionForOptimization(bar);
assertEquals("bared", bar());
ArkTools.optimizeFunctionOnNextCall(bar);
assertEquals("bared", bar());
assertOptimized(bar);
