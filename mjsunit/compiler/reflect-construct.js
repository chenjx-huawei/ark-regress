// Copyright 2017 the V8 project authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Flags: --allow-natives-syntax

// Test Reflect.construct with wrong number of arguments.
(function() {
  "use strict";
  function A() {}
  function foo() { return Reflect.construct(A); }

  ArkTools.prepareFunctionForOptimization(foo);
  assertThrows(foo);
  assertThrows(foo);
  ArkTools.optimizeFunctionOnNextCall(foo);
  assertThrows(foo);
})();
(function() {
  "use strict";
  function A(x) { this.x = x; }
  function foo() { return Reflect.construct(A, arguments); }

  ArkTools.prepareFunctionForOptimization(foo);
  assertInstanceof(foo(), A);
  assertInstanceof(foo(), A);
  assertEquals(1, foo(1).x);
  ArkTools.optimizeFunctionOnNextCall(foo);
  assertInstanceof(foo(), A);
  assertEquals(1, foo(1).x);
})();
(function() {
  "use strict";
  function A(x) { this.x = x; }
  function foo() { return Reflect.construct(A, arguments, A, A); }

  ArkTools.prepareFunctionForOptimization(foo);
  assertInstanceof(foo(), A);
  assertInstanceof(foo(), A);
  assertEquals(1, foo(1).x);
  ArkTools.optimizeFunctionOnNextCall(foo);
  assertInstanceof(foo(), A);
  assertEquals(1, foo(1).x);
})();

// Test Reflect.construct within try/catch.
(function() {
  "use strict";
  function foo(bar) {
    try {
      return Reflect.construct(bar, arguments, bar);
    } catch (e) {
      return 1;
    }
  }

  ArkTools.prepareFunctionForOptimization(foo);
  assertEquals(1, foo());
  assertEquals(1, foo());
  ArkTools.optimizeFunctionOnNextCall(foo);
  assertEquals(1, foo());
})();
(function() {
  "use strict";
  function foo(bar) {
    try {
      return Reflect.construct(bar, bar, bar);
    } catch (e) {
      return 1;
    }
  }

  ArkTools.prepareFunctionForOptimization(foo);
  assertEquals(1, foo());
  assertEquals(1, foo());
  ArkTools.optimizeFunctionOnNextCall(foo);
  assertEquals(1, foo());
})();

// Test proper order of constructor check(s) and array-like iteration.
(function() {
  var dummy_length_counter = 0;
  var dummy = { get length() { ++dummy_length_counter; return 0; } };

  function foo() {
    return Reflect.construct(undefined, dummy, undefined);
  }

  ArkTools.prepareFunctionForOptimization(foo);
  assertThrows(foo, TypeError);
  assertThrows(foo, TypeError);
  ArkTools.optimizeFunctionOnNextCall(foo);
  assertThrows(foo, TypeError);
  assertEquals(0, dummy_length_counter);
})();
(function() {
  var dummy_length_counter = 0;
  var dummy = { get length() { ++dummy_length_counter; return 0; } };

  function foo() {
    return Reflect.construct(undefined, dummy);
  }

  ArkTools.prepareFunctionForOptimization(foo);
  assertThrows(foo, TypeError);
  assertThrows(foo, TypeError);
  ArkTools.optimizeFunctionOnNextCall(foo);
  assertThrows(foo, TypeError);
  assertEquals(0, dummy_length_counter);
})();
(function() {
  var dummy_length_counter = 0;
  var dummy = { get length() { ++dummy_length_counter; return 0; } };

  function foo() {
    return Reflect.construct(null, dummy, null);
  }

  ArkTools.prepareFunctionForOptimization(foo);
  assertThrows(foo, TypeError);
  assertThrows(foo, TypeError);
  ArkTools.optimizeFunctionOnNextCall(foo);
  assertThrows(foo, TypeError);
  assertEquals(0, dummy_length_counter);
})();(function() {
  var dummy_length_counter = 0;
  var dummy = { get length() { ++dummy_length_counter; return 0; } };

  function foo() {
    return Reflect.construct(null, dummy);
  }

  ArkTools.prepareFunctionForOptimization(foo);
  assertThrows(foo, TypeError);
  assertThrows(foo, TypeError);
  ArkTools.optimizeFunctionOnNextCall(foo);
  assertThrows(foo, TypeError);
  assertEquals(0, dummy_length_counter);
})();
