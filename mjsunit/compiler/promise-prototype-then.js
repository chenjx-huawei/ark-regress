// Copyright 2018 the V8 project authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Flags: --allow-natives-syntax

(function() {
  const p = Promise.resolve(1);
  function foo(p) { return p.then(); }
  ArkTools.prepareFunctionForOptimization(foo);
  foo(p);
  foo(p);
  ArkTools.optimizeFunctionOnNextCall(foo);
  foo(p);
})();

(function() {
  const p = Promise.resolve(1);
  function foo(p) { return p.then(x => x); }
  ArkTools.prepareFunctionForOptimization(foo);
  foo(p);
  foo(p);
  ArkTools.optimizeFunctionOnNextCall(foo);
  foo(p);
})();

(function() {
  const p = Promise.resolve(1);
  function foo(p) { return p.then(x => x, y => y); }
  ArkTools.prepareFunctionForOptimization(foo);
  foo(p);
  foo(p);
  ArkTools.optimizeFunctionOnNextCall(foo);
  foo(p);
})();

(function() {
  const p = Promise.resolve(1);
  function foo(p, f) { return p.then(f, f); }
  ArkTools.prepareFunctionForOptimization(foo);
  foo(p, x => x);
  foo(p, x => x);
  ArkTools.optimizeFunctionOnNextCall(foo);
  foo(p, x => x);
})();

(function() {
  const p = Promise.resolve(1);
  function foo(p, f) { return p.then(f, f).then(f, f); }
  ArkTools.prepareFunctionForOptimization(foo);
  foo(p, x => x);
  foo(p, x => x);
  ArkTools.optimizeFunctionOnNextCall(foo);
  foo(p, x => x);
})();
