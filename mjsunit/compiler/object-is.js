// Copyright 2017 the V8 project authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Flags: --allow-natives-syntax

(function() {
  function foo(o) { return Object.is(o, -0); }
  ArkTools.prepareFunctionForOptimization(foo);
  assertTrue(foo(-0));
  assertFalse(foo(0));
  assertFalse(foo(NaN));
  assertFalse(foo(''));
  assertFalse(foo([]));
  assertFalse(foo({}));
  ArkTools.optimizeFunctionOnNextCall(foo);
  assertTrue(foo(-0));
  assertFalse(foo(0));
  assertFalse(foo(NaN));
  assertFalse(foo(''));
  assertFalse(foo([]));
  assertFalse(foo({}));
})();

(function() {
  function foo(o) { return Object.is(-0, o); }
  ArkTools.prepareFunctionForOptimization(foo);
  assertTrue(foo(-0));
  assertFalse(foo(0));
  assertFalse(foo(NaN));
  assertFalse(foo(''));
  assertFalse(foo([]));
  assertFalse(foo({}));
  ArkTools.optimizeFunctionOnNextCall(foo);
  assertTrue(foo(-0));
  assertFalse(foo(0));
  assertFalse(foo(NaN));
  assertFalse(foo(''));
  assertFalse(foo([]));
  assertFalse(foo({}));
})();

(function() {
  function foo(o) { return Object.is(+o, -0); }
  ArkTools.prepareFunctionForOptimization(foo);
  assertTrue(foo(-0));
  assertFalse(foo(0));
  assertFalse(foo(NaN));
  ArkTools.optimizeFunctionOnNextCall(foo);
  assertTrue(foo(-0));
  assertFalse(foo(0));
  assertFalse(foo(NaN));
})();

(function() {
  function foo(o) { return Object.is(-0, +o); }
  ArkTools.prepareFunctionForOptimization(foo);
  assertTrue(foo(-0));
  assertFalse(foo(0));
  assertFalse(foo(NaN));
  ArkTools.optimizeFunctionOnNextCall(foo);
  assertTrue(foo(-0));
  assertFalse(foo(0));
  assertFalse(foo(NaN));
})();

(function() {
  function foo(o) { return Object.is(o, NaN); }
  ArkTools.prepareFunctionForOptimization(foo);
  assertFalse(foo(-0));
  assertFalse(foo(0));
  assertTrue(foo(NaN));
  assertFalse(foo(''));
  assertFalse(foo([]));
  assertFalse(foo({}));
  ArkTools.optimizeFunctionOnNextCall(foo);
  assertFalse(foo(-0));
  assertFalse(foo(0));
  assertTrue(foo(NaN));
  assertFalse(foo(''));
  assertFalse(foo([]));
  assertFalse(foo({}));
})();

(function() {
  function foo(o) { return Object.is(NaN, o); }
  ArkTools.prepareFunctionForOptimization(foo);
  assertFalse(foo(-0));
  assertFalse(foo(0));
  assertTrue(foo(NaN));
  assertFalse(foo(''));
  assertFalse(foo([]));
  assertFalse(foo({}));
  ArkTools.optimizeFunctionOnNextCall(foo);
  assertFalse(foo(-0));
  assertFalse(foo(0));
  assertTrue(foo(NaN));
  assertFalse(foo(''));
  assertFalse(foo([]));
  assertFalse(foo({}));
})();

(function() {
  function foo(o) { return Object.is(+o, NaN); }
  ArkTools.prepareFunctionForOptimization(foo);
  assertFalse(foo(-0));
  assertFalse(foo(0));
  assertTrue(foo(NaN));
  ArkTools.optimizeFunctionOnNextCall(foo);
  assertFalse(foo(-0));
  assertFalse(foo(0));
  assertTrue(foo(NaN));
})();

(function() {
  function foo(o) { return Object.is(NaN, +o); }
  ArkTools.prepareFunctionForOptimization(foo);
  assertFalse(foo(-0));
  assertFalse(foo(0));
  assertTrue(foo(NaN));
  ArkTools.optimizeFunctionOnNextCall(foo);
  assertFalse(foo(-0));
  assertFalse(foo(0));
  assertTrue(foo(NaN));
})();

(function() {
  function foo(o) { return Object.is(`${o}`, "foo"); }
  ArkTools.prepareFunctionForOptimization(foo);
  assertFalse(foo("bar"));
  assertTrue(foo("foo"));
  ArkTools.optimizeFunctionOnNextCall(foo);
  assertFalse(foo("bar"));
  assertTrue(foo("foo"));
})();

(function() {
  function foo(o) { return Object.is(String(o), "foo"); }
  ArkTools.prepareFunctionForOptimization(foo);
  assertFalse(foo("bar"));
  assertTrue(foo("foo"));
  ArkTools.optimizeFunctionOnNextCall(foo);
  assertFalse(foo("bar"));
  assertTrue(foo("foo"));
})();

(function() {
  function foo(o) { return Object.is(o, o); }
  ArkTools.prepareFunctionForOptimization(foo);
  assertTrue(foo(-0));
  assertTrue(foo(0));
  assertTrue(foo(NaN));
  assertTrue(foo(''));
  assertTrue(foo([]));
  assertTrue(foo({}));
  ArkTools.optimizeFunctionOnNextCall(foo);
  assertTrue(foo(-0));
  assertTrue(foo(0));
  assertTrue(foo(NaN));
  assertTrue(foo(''));
  assertTrue(foo([]));
  assertTrue(foo({}));
})();

(function() {
  function foo(o) { return Object.is(o|0, 0); }
  ArkTools.prepareFunctionForOptimization(foo);
  assertTrue(foo(0));
  assertTrue(foo(-0));
  assertTrue(foo(NaN));
  assertFalse(foo(1));
  ArkTools.optimizeFunctionOnNextCall(foo);
  assertTrue(foo(0));
  assertTrue(foo(-0));
  assertTrue(foo(NaN));
  assertFalse(foo(1));
})();

(function() {
  const s = Symbol();
  function foo() { return Object.is(s, Symbol()); }
  ArkTools.prepareFunctionForOptimization(foo);
  assertFalse(foo());
  assertFalse(foo());
  ArkTools.optimizeFunctionOnNextCall(foo);
  assertFalse(foo());
})();

(function() {
  function foo(a, b) { return Object.is(+a, +b); }
  ArkTools.prepareFunctionForOptimization(foo);
  assertFalse(foo(1, 2));
  assertFalse(foo(0, -0));
  assertFalse(foo(-0, 0));
  assertFalse(foo(-0, 1));
  assertFalse(foo(-0, 1));
  assertFalse(foo(-Infinity, Infinity));
  assertTrue(foo(0, 0));
  assertTrue(foo(0.1, 0.1));
  assertTrue(foo(Infinity, Infinity));
  assertTrue(foo(-0, -0));
  assertTrue(foo(NaN, NaN));
  assertFalse(foo(Infinity, NaN));
  ArkTools.optimizeFunctionOnNextCall(foo);
  assertFalse(foo(1, 2));
  assertFalse(foo(0, -0));
  assertFalse(foo(-0, 0));
  assertFalse(foo(-0, 1));
  assertFalse(foo(-0, 1));
  assertFalse(foo(-Infinity, Infinity));
  assertTrue(foo(0, 0));
  assertTrue(foo(0.1, 0.1));
  assertTrue(foo(Infinity, Infinity));
  assertTrue(foo(-0, -0));
  assertTrue(foo(NaN, NaN));
  assertFalse(foo(Infinity, NaN));
})();
