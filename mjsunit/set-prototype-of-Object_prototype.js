// Copyright 2023 the V8 project authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// The typeError message returned by openharmony's SetPrototypeOf is 
// 'SetPrototypeOf: prototype set failed'.
// Change from 'Immutable prototype object \'Object.prototype\' cannot have their prototype set'
// to 'SetPrototypeOf: prototype set failed'.
function TestImmutableProtoype() {
  assertThrows(function() {
    Object.setPrototypeOf(Object.prototype, {});
  },
  TypeError,
  'SetPrototypeOf: prototype set failed');
}

TestImmutableProtoype();

Object.prototype.foo = {};

TestImmutableProtoype();
