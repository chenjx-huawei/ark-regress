# Regress

#### 介绍
Integrate V8 regression test cases.

# 执行命令

## OpenHarmony
```shell
./build.sh --product-name rk3568 --build-target ark_runtime_host_unittest --gn-args run_regress_test=true
```

## 独立编译仓
```shell
python3 ark.py x64.release regresstest
```

# 查看结果

## OpenHarmony
+ 执行结果：out/rk3568/obj/arkcompiler/ets_runtime/test/regresstest/regresstest/result.txt
+ 执行日志（具体失败原因）：out/rk3568/obj/arkcompiler/ets_runtime/test/regresstest/regresstest/test.log

## 独立编译仓
+ 执行结果：out/x64.release/regresstest/result.txt
+ 执行日志（具体失败原因）：out/x64.release/regresstest/test.log